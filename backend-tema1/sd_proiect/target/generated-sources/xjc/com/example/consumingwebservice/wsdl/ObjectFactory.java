//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.12.11 at 08:53:14 PM EET 
//


package com.example.consumingwebservice.wsdl;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.example.consumingwebservice.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.example.consumingwebservice.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetActivityRequest }
     * 
     */
    public GetActivityRequest createGetActivityRequest() {
        return new GetActivityRequest();
    }

    /**
     * Create an instance of {@link GetActivityResponse }
     * 
     */
    public GetActivityResponse createGetActivityResponse() {
        return new GetActivityResponse();
    }

    /**
     * Create an instance of {@link Activity }
     * 
     */
    public Activity createActivity() {
        return new Activity();
    }

    /**
     * Create an instance of {@link GetMedicationPlanRequest }
     * 
     */
    public GetMedicationPlanRequest createGetMedicationPlanRequest() {
        return new GetMedicationPlanRequest();
    }

    /**
     * Create an instance of {@link GetMedicationPlanResponse }
     * 
     */
    public GetMedicationPlanResponse createGetMedicationPlanResponse() {
        return new GetMedicationPlanResponse();
    }

    /**
     * Create an instance of {@link MedicationPlan }
     * 
     */
    public MedicationPlan createMedicationPlan() {
        return new MedicationPlan();
    }

}
