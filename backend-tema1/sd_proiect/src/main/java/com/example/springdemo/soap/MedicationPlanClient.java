package com.example.springdemo.soap;

import com.example.consumingwebservice.wsdl.GetActivityRequest;
import com.example.consumingwebservice.wsdl.GetActivityResponse;
import com.example.consumingwebservice.wsdl.GetMedicationPlanRequest;
import com.example.consumingwebservice.wsdl.GetMedicationPlanResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class MedicationPlanClient extends WebServiceGatewaySupport {

    public GetMedicationPlanResponse getMedicationPlansForPatient(Integer patientId) {
        GetMedicationPlanRequest request = new GetMedicationPlanRequest();
        request.setPatientId(patientId);

        GetMedicationPlanResponse response = (GetMedicationPlanResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8085/ws/activities", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetMedicationPlanRequest"));
        return response;
    }

}
