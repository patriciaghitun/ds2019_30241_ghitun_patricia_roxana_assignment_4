package com.example.springdemo.dto.medication;

import com.example.springdemo.entities.person.Patient;

public class MedicationPlanDTO {

    private Integer id;
    private Patient patient;
    private String start;
    private String end;

    public MedicationPlanDTO() {}

    public MedicationPlanDTO(Integer id, Patient patient, String start, String end) {
        this.id = id;
        this.patient = patient;
        this.start = start;
        this.end = end;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
