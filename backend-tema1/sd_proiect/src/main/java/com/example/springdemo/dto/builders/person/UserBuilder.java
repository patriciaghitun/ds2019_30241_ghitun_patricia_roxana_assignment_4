package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.UserDTO;
import com.example.springdemo.entities.person.User;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User user) {
        return new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getName(),
                user.getBirthdate(),
                user.getGender(),
                user.getAddress(),
                user.getRole());
    }

    public static User generateEntityFromDTO(UserDTO userDTO) {
        return new User(
                userDTO.getId(),
                userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.getName(),
                userDTO.getBirthdate(),
                userDTO.getGender(),
                userDTO.getAddress(),
                userDTO.getRole()
        );
    }
}
