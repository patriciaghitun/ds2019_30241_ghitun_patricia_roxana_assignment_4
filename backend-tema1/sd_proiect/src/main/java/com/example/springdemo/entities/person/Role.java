package com.example.springdemo.entities.person;

public enum Role {
    DOCTOR,
    CAREGIVER,
    PATIENT
}
