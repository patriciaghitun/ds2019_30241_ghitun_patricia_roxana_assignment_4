package com.example.springdemo.repositories;

import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.entities.medication.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {
    @Query(value = "SELECT u " +
            "FROM MedicationPlan u " +
            "ORDER BY u.id")
    List<MedicationPlan> getAllOrdered(); // by id

    MedicationPlan getMedicationPlanByPatientId(Integer id);

    List<MedicationPlan> getAllByPatientId(Integer id);
}
