package com.example.springdemo.soap;

import com.example.consumingwebservice.wsdl.GetActivityRequest;
import com.example.consumingwebservice.wsdl.GetActivityResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class ActivityClient extends WebServiceGatewaySupport {

    public GetActivityResponse getActivitiesForPatient(Integer patientId) {
        GetActivityRequest request = new GetActivityRequest();
        request.setId(patientId);

        GetActivityResponse response = (GetActivityResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8085/ws/activities", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetActivityRequest"));
        return response;
    }

}
