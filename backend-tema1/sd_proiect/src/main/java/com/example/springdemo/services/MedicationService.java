package com.example.springdemo.services;

import com.example.springdemo.dto.builders.medication.MedicationBuilder;
import com.example.springdemo.dto.medication.MedicationDTO;
import com.example.springdemo.entities.medication.Medication;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public MedicationDTO findMedicationById(Integer id){
        Optional<Medication> medication  = medicationRepository.findById(id);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "medication id", id);
        }
        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    //CRUD

    public List<MedicationDTO> findAll(){
        List<Medication> medications = medicationRepository.getAllOrdered();

        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDTO medicationDTO) {
        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }

    public Integer update(MedicationDTO medicationDTO) {

        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getId());

        if(!medication.isPresent()){
            throw new ResourceNotFoundException("medication", "medication id", medicationDTO.getId().toString());
        }
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public void delete(MedicationDTO medicationDTO){
        this.medicationRepository.deleteById(medicationDTO.getId());
    }

    public void deleteById(Integer id) {
        this.medicationRepository.deleteById(id);
    }
}
