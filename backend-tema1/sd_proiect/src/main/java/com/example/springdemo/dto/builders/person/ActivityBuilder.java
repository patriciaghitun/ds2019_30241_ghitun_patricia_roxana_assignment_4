package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.ActivityDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.entities.person.Activity;
import com.example.springdemo.entities.person.Patient;

public class ActivityBuilder {
    private ActivityBuilder() {
    }

    public static ActivityDTO generateDTOFromEntity(Activity activity) {
        return new ActivityDTO(
                activity.getId(),
                activity.getStart(),
                activity.getEnd(),
                activity.getLabel(),
                activity.getPatientId(),
                activity.getAnomalous(),
                activity.getRecommendation());
    }

    public static Activity generateEntityFromDTO(ActivityDTO activityDTO) {
        return new Activity(
                activityDTO.getId(),
                activityDTO.getStart(),
                activityDTO.getEnd(),
                activityDTO.getLabel(),
                activityDTO.getPatientId(),
                activityDTO.getAnomalous(),
                activityDTO.getRecommendation()
        );
    }
}
