package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.dto.person.UserDTO;
import com.example.springdemo.entities.person.Patient;
import com.example.springdemo.entities.person.User;

public class PatientBuilder {
    private PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient) {
        return new PatientDTO(
                patient.getId(),
                patient.getMedicalRecord(),
                patient.getUser(),
                patient.getCaregiver(),
                patient.getDoctor());
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO) {
        return new Patient(
                patientDTO.getId(),
                patientDTO.getMedicalRecord(),
                patientDTO.getUser(),
                patientDTO.getCaregiver(),
                patientDTO.getDoctor()
        );
    }
}
