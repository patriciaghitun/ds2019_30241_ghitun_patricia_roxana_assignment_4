package com.example.springdemo.services;

import com.example.springdemo.dto.builders.medication.MedicationPerPlanBuilder;
import com.example.springdemo.dto.builders.medication.MedicationPlanBuilder;
import com.example.springdemo.dto.medication.MedicationPerPlanDTO;
import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.entities.medication.MedicationPerPlan;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationPerPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPerPlanService {

    private final MedicationPerPlanRepository medicationPerPlanRepository;

    @Autowired
    public MedicationPerPlanService(MedicationPerPlanRepository medicationPerPlanRepository) {
        this.medicationPerPlanRepository = medicationPerPlanRepository;
    }

    public MedicationPerPlanDTO findMedicationPerPlanById(Integer id){
        Optional<MedicationPerPlan> medicationPerPlan  = medicationPerPlanRepository.findById(id);

        if (!medicationPerPlan.isPresent()) {
            throw new ResourceNotFoundException("medicationPerPlan", "medicationPerPlan id", id);
        }
        return MedicationPerPlanBuilder.generateDTOFromEntity(medicationPerPlan.get());
    }


    //CRUD
    public List<MedicationPerPlanDTO> findAll(){
        List<MedicationPerPlan> medicationPerPlans = medicationPerPlanRepository.getAllOrdered();

        return medicationPerPlans.stream()
                .map(MedicationPerPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    //INSERT MED TO MED PLAN!!!!
    public Integer insert(MedicationPerPlanDTO medicationPerPlanDTO) {
        return medicationPerPlanRepository
                .save(MedicationPerPlanBuilder.generateEntityFromDTO(medicationPerPlanDTO))
                .getId();
    }

    public Integer update(MedicationPerPlanDTO medicationPerPlanDTO) {

        Optional<MedicationPerPlan> medicationPerPlan = medicationPerPlanRepository.findById(medicationPerPlanDTO.getId());

        if(!medicationPerPlan.isPresent()){
            throw new ResourceNotFoundException("medicationPerPlan", "medicationPerPlan id", medicationPerPlanDTO.getId().toString());
        }
        return medicationPerPlanRepository.save(MedicationPerPlanBuilder.generateEntityFromDTO(medicationPerPlanDTO)).getId();
    }

    public void delete(MedicationPerPlanDTO medicationPerPlanDTO){
        this.medicationPerPlanRepository.deleteById(medicationPerPlanDTO.getId());
    }


    public List<MedicationPerPlanDTO> getMedicationsForPlan(Integer id) { //med plan id
        List<MedicationPerPlanDTO> medications = new ArrayList<>();
        for(MedicationPerPlan medicationPerPlan : medicationPerPlanRepository.getMedicationPerPlanByMedicationPlanId(id)) {
            medications.add(MedicationPerPlanBuilder.generateDTOFromEntity(medicationPerPlan));
        }
        return medications;
    }

}
