package com.example.springdemo.controller;

import com.example.springdemo.dto.person.ActivityDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.dto.person.UserDTO;
import com.example.springdemo.entities.person.Role;
import com.example.springdemo.entities.person.User;
import com.example.springdemo.services.ActivityService;
import com.example.springdemo.services.PatientService;
import com.example.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;
    private final ActivityService activityService;
    private final PatientService patientService;

    @Autowired
    public UserController(UserService userService, ActivityService activityService, PatientService patientService) {
        this.userService = userService;
        this.activityService= activityService;
        this.patientService = patientService;
    }

    @GetMapping(value = "/findById/{id}")
    public UserDTO findById(@PathVariable("id") Integer id){
        return userService.findUserById(id);
    }

    @GetMapping(value = "/findUserById/{id}")
    public UserDTO findUserById(@PathVariable("id") Integer id){
        return userService.findUserById(id);
    }

    @GetMapping(value = "/findByUsername/{username}")
    public UserDTO findByUsername(@PathVariable("username") String userame){
        return userService.findByUsername(userame);
    }

    //CRUD on USER
    @GetMapping(value = "/all")
    public List<UserDTO> findAll(){
        return userService.findAll();
    }

    @PostMapping(value = "/add")
    public Integer insertUserDTO(@RequestBody UserDTO userDTO){
        return userService.insert(userDTO);
    }

    @PutMapping(value = "/update")
    public Integer updateUser(@RequestBody UserDTO userDTO) {
        return userService.update(userDTO);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestBody UserDTO userDTO){
        userService.delete(userDTO);
    }

    //FUNCTIONALA!!!
    @GetMapping(value = "/loginWithCredentials/{username}/{password}")
    public void loginWithCredentials(@PathVariable("username")String username,
                                                  @PathVariable("password")String password) throws IOException, TimeoutException {

        //1. Find the user and check if it's a caregiver
        UserDTO userDTO = userService.findByUsername(username);
        if(userDTO.getRole().equals(Role.CAREGIVER) && userDTO.getPassword().equals(password)) {

            List<ActivityDTO> anomalousDTOS = activityService.findAll();

            List<ActivityDTO> caregiverActivities = new ArrayList<>();

            for(ActivityDTO activityDTO: anomalousDTOS) {
                PatientDTO patientDTO = patientService.findPatientById(activityDTO.getPatientId());
                User patientCaregiver = patientDTO.getCaregiver();

                if(patientCaregiver.getId() == userDTO.getId()) {
                    caregiverActivities.add(activityDTO);
                }
            }
            System.out.println("Activitatile unui caregiver: "+userDTO.getId());
            for(ActivityDTO activityDTO: caregiverActivities){
                System.out.println("Activitatea : "+activityDTO.getLabel());
            }
        }
    }


    @GetMapping(value = "/printActivitiesForCaregiver/{id}")
    public void printActivitiesForCaregiver(Integer caregiverId) {
        List<ActivityDTO> currentActivities = new ArrayList<>();

        List<ActivityDTO> activityDTOS = activityService.findAll();
        for(ActivityDTO activityDTO : activityDTOS){
            //pentru fiecare activitate iau id-ul pacientului
            Integer patientId = activityDTO.getPatientId();
            PatientDTO patientDTO = patientService.findPatientById(patientId);
            User caregiver = patientDTO.getCaregiver();
            if(caregiver.getId() == caregiverId) {
                //daca id-ul caregiverului gasit la activitate este acela dat, atunci il adaug
                currentActivities.add(activityDTO);
            }
        }
        System.out.println("Activitatile caregiverului cu id-ul: "+ caregiverId);
        for(ActivityDTO activityDTO: currentActivities){
            System.out.println(activityDTO.getLabel());
        }
    }

}
