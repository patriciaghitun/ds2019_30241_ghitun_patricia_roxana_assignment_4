package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.MedicationPerPlanDTO;
import com.example.springdemo.entities.medication.MedicationPerPlan;

public class MedicationPerPlanBuilder {

    private MedicationPerPlanBuilder() {
    }

    public static MedicationPerPlanDTO generateDTOFromEntity(MedicationPerPlan medicationPerPlan) {
        return new MedicationPerPlanDTO(
                medicationPerPlan.getId(),
                medicationPerPlan.getIntakemoment(),
                medicationPerPlan.getMedication(),
                medicationPerPlan.getMedicationPlan(),
                medicationPerPlan.getTaken());
    }

    public static MedicationPerPlan generateEntityFromDTO(MedicationPerPlanDTO medicationPerPlanDTO) {
        return new MedicationPerPlan(
                medicationPerPlanDTO.getId(),
                medicationPerPlanDTO.getIntakemoment(),
                medicationPerPlanDTO.getMedication(),
                medicationPerPlanDTO.getMedicationPlan(),
                medicationPerPlanDTO.getTaken()
        );
    }
}
