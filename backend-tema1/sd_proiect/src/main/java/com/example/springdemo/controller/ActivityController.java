package com.example.springdemo.controller;

import com.example.springdemo.dto.person.ActivityDTO;
import com.example.springdemo.services.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/activity")
public class ActivityController {

    private final ActivityService activityService;

    @Autowired
    public ActivityController(ActivityService activityService) {
        this.activityService = activityService;
    }


    @GetMapping(value = "/{id}")
    public ActivityDTO findById(@PathVariable("id") Integer id){
        return activityService.findActivityById(id);
    }

    @GetMapping(value = "/all")
    public List<ActivityDTO> findAll(){
        return activityService.findAll();
    }

    //CRUD
    @PostMapping(value = "/add")
    public Integer insertActivityDTO(@RequestBody ActivityDTO activityDTO){
        return activityService.insert(activityDTO);
    }



}
