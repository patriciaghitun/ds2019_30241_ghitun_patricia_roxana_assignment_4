package com.example.springdemo.repositories;

import com.example.springdemo.entities.person.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {
    @Query(value = "SELECT u " +
            "FROM Activity u " +
            "ORDER BY u.id")
    List<Activity> getAllOrdered(); // by id

    List<Activity> getAllByPatientIdOrderByStartAsc(Integer patientId);

}
