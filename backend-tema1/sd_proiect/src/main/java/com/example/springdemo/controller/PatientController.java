package com.example.springdemo.controller;


import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.services.MedicationPlanService;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public PatientController(PatientService patientService, MedicationPlanService med) {
        this.patientService = patientService;
        this.medicationPlanService = med;
    }


    @GetMapping(value = "/{id}")
    public PatientDTO findById(@PathVariable("id") Integer id){
        return patientService.findPatientById(id);
    }

    @GetMapping(value = "/findByUsername/{username}")
    public PatientDTO findByUsername(@PathVariable("username") String username){
        return patientService.findPatientByUsername(username);
    }

    @GetMapping(value = "/all")
    public List<PatientDTO> findAll(){
        return patientService.findAll();
    }

    //CRUD
    @PostMapping(value = "/add")
    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @PutMapping(value = "/update")
    public Integer updateUser(@RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestBody PatientDTO patientDTO){
        patientService.delete(patientDTO);
    }

    @GetMapping(value = "/getAllByPatientId/{id}")
    public List<MedicationPlanDTO> getAllByPatientId(@PathVariable("id") Integer id){
        return medicationPlanService.getAllByPatientId(id);
    }

    @GetMapping(value = "/getAllPatientsByCaregiverUsername/{username}")
    public List<PatientDTO> getAllPatientsByCaregiverUsername(@PathVariable("username") String username) {
        return patientService.getAllByCaregiverUsername(username);
    }

}
