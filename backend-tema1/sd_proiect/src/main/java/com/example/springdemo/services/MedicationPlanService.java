package com.example.springdemo.services;

import com.example.springdemo.dto.builders.medication.MedicationPlanBuilder;
import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public MedicationPlanDTO findMedicationPlanById(Integer id){
        Optional<MedicationPlan> medicationPlan  = medicationPlanRepository.findById(id);

        if (!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("medicationPlan", "medicationPlan id", id);
        }
        return MedicationPlanBuilder.generateDTOFromEntity(medicationPlan.get());
    }


    //CRUD
    public List<MedicationPlanDTO> findAll(){
        List<MedicationPlan> medicationPlans = medicationPlanRepository.getAllOrdered();

        return medicationPlans.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public Integer insert(MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanRepository
                .save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO))
                .getId();
    }

    public Integer update(MedicationPlanDTO medicationPlanDTO) {

        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(medicationPlanDTO.getId());

        if(!medicationPlan.isPresent()){
            throw new ResourceNotFoundException("medicationPlan", "medicationPlan id", medicationPlanDTO.getId().toString());
        }
        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getId();
    }

    public void delete(MedicationPlanDTO medicationPlanDTO){
        this.medicationPlanRepository.deleteById(medicationPlanDTO.getId());
    }


    //

    public MedicationPlanDTO getMedicationPlanForPatient(Integer patientId) {
        return MedicationPlanBuilder.generateDTOFromEntity(medicationPlanRepository.getMedicationPlanByPatientId(patientId));
    }

    public List<MedicationPlanDTO> getAllByPatientId(Integer patientId) {
        List<MedicationPlanDTO> list = new ArrayList<>();
        for(MedicationPlan medicationPlan : medicationPlanRepository.getAllByPatientId(patientId)) {
            list.add(MedicationPlanBuilder.generateDTOFromEntity(medicationPlan));
        }
        return list;
    }

}
