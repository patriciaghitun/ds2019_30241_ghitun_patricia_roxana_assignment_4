package com.example.springdemo.dto.person;

import com.example.springdemo.entities.person.Activity;
import com.example.springdemo.entities.person.Patient;

import javax.persistence.criteria.CriteriaBuilder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityDTO {
    private Integer id;
    private String start;
    private String end;

    private String label;
    private Integer patientId;

    private Boolean anomalous;
    private String recommendation;

    public ActivityDTO() {}

    public ActivityDTO(Integer id, String start, String end, String label, Integer patientId, Boolean anomalous, String recommendation) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.label = label;
        this.patientId = patientId;
        this.anomalous = anomalous;
        this.recommendation = recommendation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Boolean getAnomalous() {
        return anomalous;
    }

    public void setAnomalous(Boolean anomalous) {
        this.anomalous = anomalous;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public long getDurationByHours() throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date startDate, endDate;

        startDate = formatter.parse(this.start);
        endDate = formatter.parse(this.end);

        long diff = endDate.getTime() - startDate.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        return diffHours;
    }
}

