package com.example.springdemo;

import com.example.grpc.*;
import com.example.springdemo.dto.builders.medication.MedicationBuilder;
import com.example.springdemo.dto.builders.medication.MedicationPlanBuilder;
import com.example.springdemo.dto.medication.MedicationDTO;
import com.example.springdemo.dto.medication.MedicationPerPlanDTO;
import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.entities.medication.Medication;
import com.example.springdemo.entities.medication.MedicationPerPlan;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.entities.person.Patient;
import com.example.springdemo.entities.person.User;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.*;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@GRpcService
public class MessageServiceImpl extends MessageServiceGrpc.MessageServiceImplBase {

    private UserRepository userRepository;
    private PatientRepository patientRepository;
    private MedicationPlanRepository medicationPlanRepository;
    private MedicationPerPlanRepository medicationPerPlanRepository;
    private MedicationRepository medicationRepository;

    @Autowired
    public MessageServiceImpl(UserRepository userRepository,
                              PatientRepository patientRepository,
                              MedicationPlanRepository medicationPlanRepository,
                              MedicationPerPlanRepository medicationPerPlanRepository,
                              MedicationRepository medicationRepository) {
        this.userRepository = userRepository;
        this.patientRepository = patientRepository;
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationPerPlanRepository = medicationPerPlanRepository;
        this.medicationRepository = medicationRepository;
    }

    @Override
    public void getAllMedicationPlans(MedicationPlanRequest request, StreamObserver<MedicationPlanResponse> responseObserver) {
        System.out.println("Server - get all med plans");
        System.out.println("Received message: "+ request.getRequest());

        String clientRequest = request.getRequest();

        if(clientRequest.equals("first")) {

            System.out.println("Client sent - first request");

            User user = userRepository.findById(3).orElseThrow(
                    () -> new ResourceNotFoundException("User", "user id", 1)
            );

            //iau de la pacientul 1 - cu userul 3
            Patient patient = patientRepository.getPatientByUser(user);
            List<MedicationPlan> list = medicationPlanRepository.getAllByPatientId(patient.getId());

            System.out.println("Afisare medplans pentru pacientul :" + patient.getId());
            for(MedicationPlan medicationPlan: list) {
                System.out.println(medicationPlan.toString());
            }

//            //lista rezultata de la request
            List<MedicationPlanResponse.Builder> medicationPlanBuilderList = new ArrayList<>();
//
            List<MedPlanGrpc> medicationPlansGrpc = new ArrayList<>();
//
            MedicationPlanResponse medicationPlanResponse = null; //lista de medication plans - repeated
//
            for(MedicationPlan medicationPlan : list) {

                //iau prima data lista de medicamente din acel med plan
                List<MedGrpc> meds = new ArrayList<>();

                //pentru fiecare med - am un element in lista
                List<MedicationPerPlan> medicationPerPlans =
                        medicationPerPlanRepository.getMedicationPerPlanByMedicationPlanId(medicationPlan.getId());

                for(MedicationPerPlan medicationPerPlan : medicationPerPlans) {
                    //parcurg fiecare medicament
                    Medication currentMed = medicationPerPlan.getMedication();
                    String intakeMoment = medicationPerPlan.getIntakemoment();

                    System.out.println("MEDICATION: " + currentMed+ " din planul: "+ medicationPerPlan.getMedicationPlan().getId());

                    MedGrpc medGrpc = MedGrpc.newBuilder()
                            .setMedId(currentMed.getId())
                            .setName(currentMed.getName())
                            .setIntake(intakeMoment)
                            .build();

                    meds.add(medGrpc);
                }


                MedPlanGrpc medPlanGrpc = MedPlanGrpc.newBuilder()
                        .setMedPlanId(medicationPlan.getId())
                        .setStart(medicationPlan.getStart())
                        .setEnd(medicationPlan.getEnd())
                        .addAllMedicationsList(meds)
                        .build();
                medicationPlansGrpc.add(medPlanGrpc);
            }

            MedicationPlanResponse response = MedicationPlanResponse.newBuilder()
                    .addAllMedicationPlanList(medicationPlansGrpc)
                    .build();

            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
        else {
            System.out.println("request de la client - gresit (nu e first");
        }

    }

    @Override
    public void noticeTaken(MedStatus request, StreamObserver<ServerResponse> responseObserver) {
        String message = request.getStatus();
        System.out.println("Taken medicine :" + message);
        //add taken med to db
        //MESSAGE ESTE : medId-medPlanId
        String[] ids = message.split("-");
        String medIdString = ids[0];
        String medPlanIdString = ids[1];

        //gasirea medicamentului cu id-ul respectiv si setarea faptului ca este taken

//        Optional<Medication> receivedMedOptional = medicationRepository.findById(Integer.parseInt(medIdString));
//        Optional<MedicationPlan> receivedMedPlanOptional = medicationPlanRepository.findById(Integer.parseInt(medPlanIdString));

//        MedicationDTO medicationDTO = MedicationBuilder.generateDTOFromEntity(receivedMedOptional.get());
//        MedicationPlanDTO medPlanDTO = MedicationPlanBuilder.generateDTOFromEntity(receivedMedPlanOptional.get());

        List<MedicationPerPlan> medicationPerPlanDTOS = medicationPerPlanRepository
                .getAllByMedicationPlanIdAndMedicationId(
                        Integer.parseInt(medPlanIdString),
                        Integer.parseInt(medIdString));

        System.out.println("medication per plans cu med id: "+ medIdString + " si medPlanId: "+ medPlanIdString);
        System.out.println(Arrays.asList(medicationPerPlanDTOS));

        MedicationPerPlan updatedMedPerPlan = new MedicationPerPlan();

        if(medicationPerPlanDTOS.size() == 1) {
            updatedMedPerPlan = medicationPerPlanDTOS.get(0);
        }
        updatedMedPerPlan.setTaken(true);

        medicationPerPlanRepository.save(updatedMedPerPlan);

        ServerResponse serverResponse = ServerResponse.newBuilder()
                .setServerResponse("Serverul a primit - med taken :"+ message)
                .build();

        responseObserver.onNext(serverResponse);
        responseObserver.onCompleted();
    }

    @Override
    public void noticeNotTaken(MedStatus request, StreamObserver<ServerResponse> responseObserver) {
        String message = request.getStatus();
        System.out.println("NOT taken medicine :" + message);
        //add not taken med to db
        ServerResponse serverResponse = ServerResponse.newBuilder()
                .setServerResponse("Serverul a primit - med NOT taken :"+ message)
                .build();
        responseObserver.onNext(serverResponse);
        responseObserver.onCompleted();
    }
}