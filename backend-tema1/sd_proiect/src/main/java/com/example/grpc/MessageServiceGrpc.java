package com.example.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: Message.proto")
public final class MessageServiceGrpc {

  private MessageServiceGrpc() {}

  public static final String SERVICE_NAME = "MessageService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.example.grpc.MedStatus,
      com.example.grpc.ServerResponse> getNoticeTakenMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "noticeTaken",
      requestType = com.example.grpc.MedStatus.class,
      responseType = com.example.grpc.ServerResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.MedStatus,
      com.example.grpc.ServerResponse> getNoticeTakenMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.MedStatus, com.example.grpc.ServerResponse> getNoticeTakenMethod;
    if ((getNoticeTakenMethod = MessageServiceGrpc.getNoticeTakenMethod) == null) {
      synchronized (MessageServiceGrpc.class) {
        if ((getNoticeTakenMethod = MessageServiceGrpc.getNoticeTakenMethod) == null) {
          MessageServiceGrpc.getNoticeTakenMethod = getNoticeTakenMethod = 
              io.grpc.MethodDescriptor.<com.example.grpc.MedStatus, com.example.grpc.ServerResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MessageService", "noticeTaken"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.MedStatus.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.ServerResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MessageServiceMethodDescriptorSupplier("noticeTaken"))
                  .build();
          }
        }
     }
     return getNoticeTakenMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.MedStatus,
      com.example.grpc.ServerResponse> getNoticeNotTakenMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "noticeNotTaken",
      requestType = com.example.grpc.MedStatus.class,
      responseType = com.example.grpc.ServerResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.MedStatus,
      com.example.grpc.ServerResponse> getNoticeNotTakenMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.MedStatus, com.example.grpc.ServerResponse> getNoticeNotTakenMethod;
    if ((getNoticeNotTakenMethod = MessageServiceGrpc.getNoticeNotTakenMethod) == null) {
      synchronized (MessageServiceGrpc.class) {
        if ((getNoticeNotTakenMethod = MessageServiceGrpc.getNoticeNotTakenMethod) == null) {
          MessageServiceGrpc.getNoticeNotTakenMethod = getNoticeNotTakenMethod = 
              io.grpc.MethodDescriptor.<com.example.grpc.MedStatus, com.example.grpc.ServerResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MessageService", "noticeNotTaken"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.MedStatus.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.ServerResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MessageServiceMethodDescriptorSupplier("noticeNotTaken"))
                  .build();
          }
        }
     }
     return getNoticeNotTakenMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.example.grpc.MedicationPlanRequest,
      com.example.grpc.MedicationPlanResponse> getGetAllMedicationPlansMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getAllMedicationPlans",
      requestType = com.example.grpc.MedicationPlanRequest.class,
      responseType = com.example.grpc.MedicationPlanResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.grpc.MedicationPlanRequest,
      com.example.grpc.MedicationPlanResponse> getGetAllMedicationPlansMethod() {
    io.grpc.MethodDescriptor<com.example.grpc.MedicationPlanRequest, com.example.grpc.MedicationPlanResponse> getGetAllMedicationPlansMethod;
    if ((getGetAllMedicationPlansMethod = MessageServiceGrpc.getGetAllMedicationPlansMethod) == null) {
      synchronized (MessageServiceGrpc.class) {
        if ((getGetAllMedicationPlansMethod = MessageServiceGrpc.getGetAllMedicationPlansMethod) == null) {
          MessageServiceGrpc.getGetAllMedicationPlansMethod = getGetAllMedicationPlansMethod = 
              io.grpc.MethodDescriptor.<com.example.grpc.MedicationPlanRequest, com.example.grpc.MedicationPlanResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MessageService", "getAllMedicationPlans"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.MedicationPlanRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.grpc.MedicationPlanResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new MessageServiceMethodDescriptorSupplier("getAllMedicationPlans"))
                  .build();
          }
        }
     }
     return getGetAllMedicationPlansMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MessageServiceStub newStub(io.grpc.Channel channel) {
    return new MessageServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MessageServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new MessageServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MessageServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new MessageServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class MessageServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void noticeTaken(com.example.grpc.MedStatus request,
        io.grpc.stub.StreamObserver<com.example.grpc.ServerResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getNoticeTakenMethod(), responseObserver);
    }

    /**
     */
    public void noticeNotTaken(com.example.grpc.MedStatus request,
        io.grpc.stub.StreamObserver<com.example.grpc.ServerResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getNoticeNotTakenMethod(), responseObserver);
    }

    /**
     */
    public void getAllMedicationPlans(com.example.grpc.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.MedicationPlanResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllMedicationPlansMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getNoticeTakenMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.MedStatus,
                com.example.grpc.ServerResponse>(
                  this, METHODID_NOTICE_TAKEN)))
          .addMethod(
            getNoticeNotTakenMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.MedStatus,
                com.example.grpc.ServerResponse>(
                  this, METHODID_NOTICE_NOT_TAKEN)))
          .addMethod(
            getGetAllMedicationPlansMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.grpc.MedicationPlanRequest,
                com.example.grpc.MedicationPlanResponse>(
                  this, METHODID_GET_ALL_MEDICATION_PLANS)))
          .build();
    }
  }

  /**
   */
  public static final class MessageServiceStub extends io.grpc.stub.AbstractStub<MessageServiceStub> {
    private MessageServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MessageServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MessageServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MessageServiceStub(channel, callOptions);
    }

    /**
     */
    public void noticeTaken(com.example.grpc.MedStatus request,
        io.grpc.stub.StreamObserver<com.example.grpc.ServerResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNoticeTakenMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void noticeNotTaken(com.example.grpc.MedStatus request,
        io.grpc.stub.StreamObserver<com.example.grpc.ServerResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getNoticeNotTakenMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getAllMedicationPlans(com.example.grpc.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<com.example.grpc.MedicationPlanResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetAllMedicationPlansMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MessageServiceBlockingStub extends io.grpc.stub.AbstractStub<MessageServiceBlockingStub> {
    private MessageServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MessageServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MessageServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MessageServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.example.grpc.ServerResponse noticeTaken(com.example.grpc.MedStatus request) {
      return blockingUnaryCall(
          getChannel(), getNoticeTakenMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.ServerResponse noticeNotTaken(com.example.grpc.MedStatus request) {
      return blockingUnaryCall(
          getChannel(), getNoticeNotTakenMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.example.grpc.MedicationPlanResponse getAllMedicationPlans(com.example.grpc.MedicationPlanRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetAllMedicationPlansMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MessageServiceFutureStub extends io.grpc.stub.AbstractStub<MessageServiceFutureStub> {
    private MessageServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MessageServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MessageServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MessageServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.ServerResponse> noticeTaken(
        com.example.grpc.MedStatus request) {
      return futureUnaryCall(
          getChannel().newCall(getNoticeTakenMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.ServerResponse> noticeNotTaken(
        com.example.grpc.MedStatus request) {
      return futureUnaryCall(
          getChannel().newCall(getNoticeNotTakenMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.grpc.MedicationPlanResponse> getAllMedicationPlans(
        com.example.grpc.MedicationPlanRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetAllMedicationPlansMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_NOTICE_TAKEN = 0;
  private static final int METHODID_NOTICE_NOT_TAKEN = 1;
  private static final int METHODID_GET_ALL_MEDICATION_PLANS = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MessageServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MessageServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_NOTICE_TAKEN:
          serviceImpl.noticeTaken((com.example.grpc.MedStatus) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.ServerResponse>) responseObserver);
          break;
        case METHODID_NOTICE_NOT_TAKEN:
          serviceImpl.noticeNotTaken((com.example.grpc.MedStatus) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.ServerResponse>) responseObserver);
          break;
        case METHODID_GET_ALL_MEDICATION_PLANS:
          serviceImpl.getAllMedicationPlans((com.example.grpc.MedicationPlanRequest) request,
              (io.grpc.stub.StreamObserver<com.example.grpc.MedicationPlanResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class MessageServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    MessageServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.example.grpc.Message.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("MessageService");
    }
  }

  private static final class MessageServiceFileDescriptorSupplier
      extends MessageServiceBaseDescriptorSupplier {
    MessageServiceFileDescriptorSupplier() {}
  }

  private static final class MessageServiceMethodDescriptorSupplier
      extends MessageServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    MessageServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MessageServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MessageServiceFileDescriptorSupplier())
              .addMethod(getNoticeTakenMethod())
              .addMethod(getNoticeNotTakenMethod())
              .addMethod(getGetAllMedicationPlansMethod())
              .build();
        }
      }
    }
    return result;
  }
}
