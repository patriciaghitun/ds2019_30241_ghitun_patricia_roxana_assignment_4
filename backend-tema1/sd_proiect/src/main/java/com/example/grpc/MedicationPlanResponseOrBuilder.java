// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Message.proto

package com.example.grpc;

public interface MedicationPlanResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:MedicationPlanResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   *lista de med plans
   *repetead ca sa ia toata lista deodata
   * </pre>
   *
   * <code>repeated .MedPlanGrpc medicationPlanList = 1;</code>
   */
  java.util.List<com.example.grpc.MedPlanGrpc> 
      getMedicationPlanListList();
  /**
   * <pre>
   *lista de med plans
   *repetead ca sa ia toata lista deodata
   * </pre>
   *
   * <code>repeated .MedPlanGrpc medicationPlanList = 1;</code>
   */
  com.example.grpc.MedPlanGrpc getMedicationPlanList(int index);
  /**
   * <pre>
   *lista de med plans
   *repetead ca sa ia toata lista deodata
   * </pre>
   *
   * <code>repeated .MedPlanGrpc medicationPlanList = 1;</code>
   */
  int getMedicationPlanListCount();
  /**
   * <pre>
   *lista de med plans
   *repetead ca sa ia toata lista deodata
   * </pre>
   *
   * <code>repeated .MedPlanGrpc medicationPlanList = 1;</code>
   */
  java.util.List<? extends com.example.grpc.MedPlanGrpcOrBuilder> 
      getMedicationPlanListOrBuilderList();
  /**
   * <pre>
   *lista de med plans
   *repetead ca sa ia toata lista deodata
   * </pre>
   *
   * <code>repeated .MedPlanGrpc medicationPlanList = 1;</code>
   */
  com.example.grpc.MedPlanGrpcOrBuilder getMedicationPlanListOrBuilder(
      int index);
}
