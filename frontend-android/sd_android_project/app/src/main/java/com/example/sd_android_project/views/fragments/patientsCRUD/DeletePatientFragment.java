package com.example.sd_android_project.views.fragments.patientsCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;

public class DeletePatientFragment extends Fragment {

    private EditText id;
    private Button delete;

    DoctorController doctorController = new DoctorController();

    public static DeletePatientFragment newInstance() {

        Bundle args = new Bundle();

        DeletePatientFragment fragment = new DeletePatientFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_delete_patient, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        id = view.findViewById(R.id.delete_patient_id);
        delete = view.findViewById(R.id.delete_patient);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer patientId = Integer.parseInt(id.getText().toString().trim());
                deletePatientById(patientId);
            }
        });
    }

    private void deletePatientById(Integer id){
        doctorController.deletePatientById(this.getContext(),id);
    }

}
