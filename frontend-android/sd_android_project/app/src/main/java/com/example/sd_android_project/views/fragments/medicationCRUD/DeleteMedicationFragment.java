package com.example.sd_android_project.views.fragments.medicationCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;

public class DeleteMedicationFragment extends Fragment {

    private EditText id;
    private Button delete;

    DoctorController doctorController = new DoctorController();

    public static DeleteMedicationFragment newInstance() {

        Bundle args = new Bundle();

        DeleteMedicationFragment fragment = new DeleteMedicationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_delete_medication, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        id = view.findViewById(R.id.delete_med_id);
        delete = view.findViewById(R.id.delete_medication);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer medId = Integer.parseInt(id.getText().toString().trim());
                deleteMedById(medId);
            }
        });
    }

    private void deleteMedById(Integer id){
        doctorController.deleteMedicationById(this.getContext(),id);
    }

}
