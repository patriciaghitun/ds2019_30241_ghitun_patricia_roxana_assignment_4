package com.example.sd_android_project.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.user.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class CaregiverAdapter extends RecyclerView.Adapter<CaregiverAdapter.CaregiverViewHolder>{

    private static final String TAG = "CaregiverAdapter";

    private List<UserDTO> caregiversDTO = new ArrayList<>();
    private Context context ;

    public CaregiverAdapter(List<UserDTO> caregiversDTO, Context context) {
        this.caregiversDTO = caregiversDTO;
        this.context = context;
    }

    @NonNull
    @Override
    public CaregiverViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.allcaregivers_recycler_item, parent, false);
        CaregiverAdapter.CaregiverViewHolder caregiverViewHolder = new CaregiverAdapter.CaregiverViewHolder(view);
        return caregiverViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CaregiverViewHolder holder, int position) {
        holder.name.setText(caregiversDTO.get(position).getName());
        holder.username.setText(caregiversDTO.get(position).getUsername());

        for(UserDTO u: caregiversDTO) {
            System.out.println(u.getUsername());
        }
    }


    @Override
    public int getItemCount() {
        System.out.println("adapter - size"+caregiversDTO.size());
        return caregiversDTO.size();
    }

    public class CaregiverViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView username;

        LinearLayout allCaregiversLayout;

        public CaregiverViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.recycler_caregiver_name);
            username = itemView.findViewById(R.id.recycler_caregiver_username);

            allCaregiversLayout = itemView.findViewById(R.id.allcaregivers_layout);
        }
    }
}
