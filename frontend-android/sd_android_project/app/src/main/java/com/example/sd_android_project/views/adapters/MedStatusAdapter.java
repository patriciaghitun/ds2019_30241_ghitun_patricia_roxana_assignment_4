package com.example.sd_android_project.views.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.medication.MedicationDTO;

import java.util.ArrayList;
import java.util.List;

public class MedStatusAdapter extends RecyclerView.Adapter<MedStatusAdapter.MedStatusViewHolder> {

    private static final String TAG = "MedStatusAdapter";

    private DoctorController doctorController = new DoctorController();

    private List<MedicationDTO> medicationDTOS = new ArrayList<>();
    private Context context ;
    private List<Boolean> medsTakenStatus;
    private List<String> medsIntakes;

    public MedStatusAdapter(List<MedicationDTO> medicationDTOS, Context context, List<Boolean> m, List<String> intakes) {
        this.medicationDTOS = medicationDTOS;
        this.context = context;
        this.medsTakenStatus = m;
        this.medsIntakes = intakes;
    }

    @NonNull
    @Override
    public MedStatusViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.allmedplanstatus_meds_recycler_item, parent, false);

        MedStatusAdapter.MedStatusViewHolder medStatusViewHolder =
                new MedStatusAdapter.MedStatusViewHolder(view);

        return medStatusViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MedStatusViewHolder holder, final int position) {

        holder.id.setText("Id: " + medicationDTOS.get(position).getId());
        holder.name.setText("Name: " + medicationDTOS.get(position).getName());
        holder.dosage.setText("Dosage: " + medicationDTOS.get(position).getDosage());
        holder.sideEffects.setText("Side effects: " + medicationDTOS.get(position).getSide_effects());
        holder.taken.setText("Taken: " + medsTakenStatus.get(position).toString());
        holder.intake.setText("Intake moment: " + medsIntakes.get(position));
    }

    @Override
    public int getItemCount() {
        return medicationDTOS.size();
    }

    public class MedStatusViewHolder extends RecyclerView.ViewHolder {

        TextView id, name, dosage, sideEffects, taken, intake;

        LinearLayout allMedPlanStatusMedsLayout;

        public MedStatusViewHolder(@NonNull View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.status_meds_id);
            name = itemView.findViewById(R.id.status_meds_name);
            dosage = itemView.findViewById(R.id.status_meds_dosage);
            sideEffects = itemView.findViewById(R.id.status_meds_side_effects);
            taken = itemView.findViewById(R.id.status_meds_taken);
            intake = itemView.findViewById(R.id.status_meds_intake);

            allMedPlanStatusMedsLayout = itemView.findViewById(R.id.allmedplanstatusmeds_layout);
        }
    }

}
