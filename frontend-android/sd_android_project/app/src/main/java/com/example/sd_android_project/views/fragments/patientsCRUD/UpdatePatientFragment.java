package com.example.sd_android_project.views.fragments.patientsCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;

public class UpdatePatientFragment extends Fragment {

    Button updateMedicalRecord, updateUserInfo, updateCaregiver, updateDoctor;

    public static UpdatePatientFragment newInstance() {

        Bundle args = new Bundle();

        UpdatePatientFragment fragment = new UpdatePatientFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_patient, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        updateMedicalRecord = view.findViewById(R.id.update_patient_medical_info);

        updateUserInfo = view.findViewById(R.id.update_patient_user_info);
        updateCaregiver = view.findViewById(R.id.update_patient_caregiver);
        updateDoctor = view.findViewById(R.id.update_patient_doctor);

        updateMedicalRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goToUpdateMedicalRecord();
            }
        });
    }

    private void goToUpdateMedicalRecord() {

        this.getFragmentManager().beginTransaction()
                .add(R.id.update_patient_frame, UpdateRecordPatientFragment.newInstance()).commit();
    }
}
