package com.example.sd_android_project.views.fragments.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.user.ActivityDTO;
import com.example.sd_android_project.services.DoctorService;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChartFragment extends Fragment {

    private static final String TAG = "ActivityChartFragment";

    private TextView activityInfo;
    private Integer patientId;
    private String day;

    private List<ActivityDTO> activitiesToShow = new ArrayList<>();

    private PieChart pieChart ;


    PieDataSet pieDataSet ;
    PieData pieData ;

    public static ActivityChartFragment newInstance() {

        Bundle args = new Bundle();

        ActivityChartFragment fragment = new ActivityChartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_activity_chart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.patientId = getArguments().getInt("patientId");
        this.day = getArguments().getString("day");

        System.out.println("PATIENT ID:"+ patientId + " - " + day);

        activityInfo = view.findViewById(R.id.chart_activity_info);
        activityInfo.setText("Patient: "+  this.patientId + " on day:"+ this.day);

        pieChart = view.findViewById(R.id.activities_chart);
        pieChart.setUsePercentValues(true);


        final Call<List<ActivityDTO>> activities = DoctorService.getInstance().getActivitiesForPatientWithSOAP(patientId);
                activities.enqueue(new Callback<List<ActivityDTO>>() {
                    @Override
                    public void onResponse(Call<List<ActivityDTO>> call, Response<List<ActivityDTO>> response) {
                        Log.d(TAG, "onResponse: ");
                        if(response.isSuccessful()) {
                            Log.d(TAG, "onResponse: successful :" + response.body());
                            if(!response.body().isEmpty()) {
                                for(ActivityDTO currentActivity: response.body()) {
                                    if(checkActivityDay(currentActivity)) {
                                        activitiesToShow.add(currentActivity);
                                    }
                                }
                                ArrayList<Entry> entries = new ArrayList<>();
                                ArrayList<String> pieEntryLabels = new ArrayList<>();
                                Map<String, Float> activityData = new HashMap<>();


                                for(ActivityDTO currentActivity : activitiesToShow) {
                                    System.out.println(currentActivity.toString());

                                    try {
                                        System.out.println("- MINUTE: "+ currentActivity.getDurationByMinutes().floatValue());

                                        if(activityData.containsKey(currentActivity.getLabel())) {
                                            float currentMin = activityData.get(currentActivity.getLabel());

                                            activityData.put(currentActivity.getLabel(),
                                                    currentMin + currentActivity.getDurationByMinutes().floatValue());
                                        } else {
                                            activityData.put(currentActivity.getLabel(),
                                                    calculateFloatMins(currentActivity.getDurationByMinutes()));
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }

                                System.out.println("AFISARE HASHMAP:");
                                System.out.println(Arrays.asList(activityData));

                                int index = 0;
                                int entryCount = 0;
                                for(Map.Entry<String, Float> entry: activityData.entrySet()) {

                                    entryCount++;
                                    entries.add(new BarEntry(entry.getValue(), index++));
                                    pieEntryLabels.add(entry.getKey());

                                }

                                System.out.println("ENTRY COUNT: "+ entryCount);

                                pieDataSet = new PieDataSet(entries, "");
                                pieData = new PieData(pieEntryLabels, pieDataSet);
                                pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                                pieChart.setData(pieData);
                                pieChart.animateY(3000);

                            } else {
                                Toast.makeText(view.getContext(), "Nu exista activitati pentru acest pacient!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.d(TAG, "onResponse: failed");
                            Toast.makeText(view.getContext(), "Nu s-au putut lua activitatile pentru acest pacient!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<List<ActivityDTO>> call, Throwable t) {
                        Log.d(TAG, "onFailure: ");
                        Toast.makeText(getContext(), "Get Activities - onFailure", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public float calculateFloatMins(Double min) {
        String minString = String.valueOf(min);
        return Float.parseFloat(minString);
    }

    public Boolean checkActivityDay(ActivityDTO activityDTO) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date activityStartDate = new Date();

        Integer dayInt = Integer.parseInt(this.day);
        try {
            activityStartDate = activityDTO.getStartDate();

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Bucharest"));
            cal.setTime(activityStartDate);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            if (day != dayInt) {
                return false;
            } else {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


}
