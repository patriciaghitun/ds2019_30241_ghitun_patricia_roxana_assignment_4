package com.example.sd_android_project.views.fragments.medicationCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.medication.MedicationDTO;

public class UpdateMedicationFragment extends Fragment {

    private EditText id, name, sideEffects, dosage;
    private Button update;

    DoctorController doctorController = new DoctorController();

    public static UpdateMedicationFragment newInstance() {

        Bundle args = new Bundle();

        UpdateMedicationFragment fragment = new UpdateMedicationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_medication, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        id = view.findViewById(R.id.update_med_id);
        name = view.findViewById(R.id.update_med_name);
        sideEffects = view.findViewById(R.id.update_med_sideEffects);
        dosage = view.findViewById(R.id.update_med_dosage);

        update = view.findViewById(R.id.update_medication);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer medId = Integer.parseInt(id.getText().toString().trim());
                Integer dosageInt = Integer.parseInt(dosage.getText().toString().trim());
                MedicationDTO medicationDTO = new MedicationDTO(
                        medId,
                        name.getText().toString().trim(),
                        sideEffects.getText().toString().trim(),
                        dosageInt
                        );

                updateMedication(medicationDTO);
            }
        });

    }

    private void updateMedication(MedicationDTO medicationDTO) {
        doctorController.updateMedication(this.getContext(), medicationDTO);
    }
}
