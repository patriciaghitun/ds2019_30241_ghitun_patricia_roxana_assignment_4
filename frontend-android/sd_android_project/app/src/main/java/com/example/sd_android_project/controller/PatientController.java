package com.example.sd_android_project.controller;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.builders.UserBuilder;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.dto.user.UserDTO;
import com.example.sd_android_project.services.DoctorService;
import com.example.sd_android_project.services.PatientService;
import com.example.sd_android_project.services.UserService;
import com.example.sd_android_project.views.adapters.MedPlansAdapter;
import com.example.sd_android_project.views.adapters.PatientAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientController {

    public void getAllMedPlans(final Context context, Integer patientId, final View view) {

       Call<List<MedicationPlanDTO>> medList = PatientService.getInstance().getAllByPatientId(patientId);
       medList.enqueue(new Callback<List<MedicationPlanDTO>>() {
           @Override
           public void onResponse(Call<List<MedicationPlanDTO>> call, Response<List<MedicationPlanDTO>> response) {

               RecyclerView recyclerView = view.findViewById(R.id.recycler_view_med_plans);
               MedPlansAdapter adapter = new MedPlansAdapter(response.body(), context);

               recyclerView.setAdapter(adapter);
               recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
           }

           @Override
           public void onFailure(Call<List<MedicationPlanDTO>> call, Throwable t) {

           }
       });
    }

}
