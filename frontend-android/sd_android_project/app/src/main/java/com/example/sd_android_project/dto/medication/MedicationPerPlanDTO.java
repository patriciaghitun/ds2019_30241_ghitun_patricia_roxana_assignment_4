package com.example.sd_android_project.dto.medication;


import com.example.sd_android_project.entities.Medication;
import com.example.sd_android_project.entities.MedicationPlan;

public class MedicationPerPlanDTO {
    private Integer id;
    private String intakemoment;
    private Medication medication;
    private MedicationPlan medicationPlan;
    private Boolean taken;

    public MedicationPerPlanDTO(Integer id, String intakemoment, Medication medication, MedicationPlan medicationPlan) {
        this.id = id;
        this.intakemoment = intakemoment;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
    }

    public MedicationPerPlanDTO(String intakemoment, Medication medication, MedicationPlan medicationPlan) {
        this.intakemoment = intakemoment;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
    }

    public MedicationPerPlanDTO(Integer id, String intakemoment, Medication medication, MedicationPlan medicationPlan, Boolean taken) {
        this.id = id;
        this.intakemoment = intakemoment;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
        this.taken = taken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntakemoment() {
        return intakemoment;
    }

    public void setIntakemoment(String intakemoment) {
        this.intakemoment = intakemoment;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }

    @Override
    public String toString() {
        return "MedicationPerPlanDTO{" +
                "id=" + id +
                ", intakemoment='" + intakemoment + '\'' +
                ", medication=" + medication +
                ", medicationPlan=" + medicationPlan +
                ", taken=" + taken +
                '}';
    }
}