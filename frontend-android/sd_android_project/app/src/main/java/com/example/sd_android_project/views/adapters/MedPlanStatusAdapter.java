package com.example.sd_android_project.views.adapters;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.views.activities.AddMedicationPlan;
import com.example.sd_android_project.views.activities.MedStatusActivity;

import java.util.ArrayList;
import java.util.List;


public class MedPlanStatusAdapter extends RecyclerView.Adapter<MedPlanStatusAdapter.MedPlanStatusViewHolder> {

    private static final String TAG = "MedPlanStatusAdapter";
    private DoctorController doctorController = new DoctorController();

    private List<MedicationPlanDTO> medicationPlanDTOList = new ArrayList<>();
    private Context context ;

    public MedPlanStatusAdapter(List<MedicationPlanDTO> medicationPlanDTOS, Context context) {
        this.medicationPlanDTOList = medicationPlanDTOS;
        this.context = context;
    }

    @NonNull
    @Override
    public MedPlanStatusViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.allmedplansstatus_recycler_item, parent, false);

        MedPlanStatusAdapter.MedPlanStatusViewHolder medPlanStatusViewHolder =
                new MedPlanStatusAdapter.MedPlanStatusViewHolder(view);

        return medPlanStatusViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MedPlanStatusViewHolder holder, final int position) {

        holder.id.setText("Id: " + medicationPlanDTOList.get(position).getId());
        holder.start.setText("Start: "+ medicationPlanDTOList.get(position).getStart());
        holder.end.setText("End: "+ medicationPlanDTOList.get(position).getEnd());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("You selected on med plan-"+ holder.id.getText().toString());

//                MedStatusActivity medStatusFragment = new MedStatusActivity();
//                medStatusFragment.setArguments(bundle);
//
//                DoctorActivity doctorActivity = new DoctorActivity();
//                        doctorActivity
//                                .getSupportFragmentManager().beginTransaction()
//                        .add(R.id.doctor_patient_frame, medStatusFragment)
//                        .addToBackStack("dogList")
//                        .commit();

                Intent intent = new Intent(context, MedStatusActivity.class);
                intent.putExtra("medicationPlanId", medicationPlanDTOList.get(position).getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return medicationPlanDTOList.size();
    }

    public class MedPlanStatusViewHolder extends RecyclerView.ViewHolder {

        TextView id;
        TextView start;
        TextView end;

        LinearLayout allMedPlanStatusLayout;

        public MedPlanStatusViewHolder(@NonNull View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.status_med_plan_id);
            start = itemView.findViewById(R.id.status_med_plan_start);
            end = itemView.findViewById(R.id.status_med_plan_end);

            allMedPlanStatusLayout = itemView.findViewById(R.id.allmedplansstatus_layout);
        }
    }

}
