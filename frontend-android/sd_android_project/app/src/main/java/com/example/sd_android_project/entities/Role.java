package com.example.sd_android_project.entities;

public enum Role {
    DOCTOR,
    CAREGIVER,
    PATIENT
}
