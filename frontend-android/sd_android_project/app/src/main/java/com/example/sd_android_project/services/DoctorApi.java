package com.example.sd_android_project.services;

import android.widget.ListView;

import com.example.sd_android_project.dto.medication.MedicationDTO;
import com.example.sd_android_project.dto.medication.MedicationPerPlanDTO;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.dto.user.ActivityDTO;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.dto.user.UserDTO;

import java.util.List;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface DoctorApi {

    //CRUD ON PATIENTS
    @GET("doctor/patient/findPatientById/{id}")
    Call<PatientDTO> findPatientById(@Path("id") Integer id);

    @POST("doctor/patient/add")
    Call<Integer> insertPatientDTO(@Body PatientDTO patientDTO);

    @GET("doctor/patient/all")
    Call<List<PatientDTO>> getAllPatients();

    @PUT("doctor/patient/update")
    Call<Integer> updatePatientDTO(@Body PatientDTO patientDTO);

    @DELETE("doctor/patient/deleteById/{id}")
    Call<ResponseBody> deletePatientById(@Path("id") Integer id);


    //CRUD MEDICATION
    @GET("doctor/medication/all")
    Call<List<MedicationDTO>> getAllMedications();

    @POST("doctor/medication/add")
    Call<Integer> insertMedicationDTO(@Body MedicationDTO medicationDTO);

    @PUT("doctor/medication/update")
    Call<Integer> updateMedicationDTO(@Body MedicationDTO medicationDTO);

    @DELETE("doctor/medication/delete")
    Call<Void> deleteMedicationDTO(@Body MedicationDTO medicationDTO);

    @DELETE("doctor/medication/deleteById/{id}")
    Call<ResponseBody> deleteMedicationDTOById(@Path("id") Integer id);

    //CRUD CAREGIVERS
    @POST("doctor/caregiver/add")
    Call<Integer> insertCaregiverDTO(@Body UserDTO userDTO);

    @GET("doctor/caregiver/all")
    Call<List<UserDTO>> getAllCaregivers();

    @PUT("doctor/caregiver/update")
    Call<Integer> updateCaregiver(@Body UserDTO userDTO);

    @DELETE("doctor/caregiver/deleteById/{id}")
    Call<ResponseBody> deleteCaregiverById(@Path("id") Integer id);

    //CREATE MEDICATION PLAN
    @POST("doctor/createMedicationPlan")
    Call<Integer> createMedicationPlan(@Body MedicationPlanDTO medicationPlanDTO);

    @GET("doctor/getMedicationPlanById/{id}")
    Call<MedicationPlanDTO> getMedicationPlanById(@Path("id") Integer id);

    @POST("doctor/addMedicationToPlan")
    Call<Integer> addMedicationToPlan(@Body MedicationPerPlanDTO medicationPerPlanDTO);

    @GET("doctor/getMedicationsForPlan/{id}")
    Call<List<MedicationPerPlanDTO>> getMedicationsForPlan(@Path("id") Integer id);

    @GET("doctor/patient/getAllPatientsForCaregiver/{id}")
    List<PatientDTO> getAllPatientsForCaregiver(@Path("id") Integer id);

    //ACTIVITIY STUFF + SOAP

    @GET("doctor/soap/getActivitiesForPatient/{id}")
    Call<List<ActivityDTO>> getActivitiesForPatientWithSOAP(@Path("id") Integer id);

    @PUT("doctor/setRecommendationForActivity/{id}")
    Call<Integer> setRecommendationForActivity(@Path("id") Integer id, @Body String recommendation);

    @PUT("doctor/changeActivityAnomalousStatus/{id}")
    Call<Integer> changeActivityAnomalousStatus(@Path("id") Integer id, @Body Boolean anomalous);

    //MEDICATION PLAN SOAP
    @GET("doctor/soap/getMedPlansForPatient/{id}")
    Call<List<MedicationPlanDTO>> getMedPlansForPatientWithSOAP(@Path("id") Integer id);

}
