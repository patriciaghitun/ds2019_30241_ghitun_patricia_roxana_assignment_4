package com.example.sd_android_project.views.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.user.ActivityDTO;

import java.util.ArrayList;
import java.util.List;

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesAdapter.ActivitiesViewHolder> {
    private static final String TAG = "ActivitiesAdapter";


    private DoctorController doctorController = new DoctorController();

    private List<ActivityDTO> activityDTOS = new ArrayList<>();
    private Context context ;

    public ActivitiesAdapter(List<ActivityDTO> activityDTOS, Context context) {
        this.activityDTOS = activityDTOS;
        this.context = context;
    }

    @NonNull
    @Override
    public ActivitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.allactivities_recycler_item, parent, false);
        ActivitiesAdapter.ActivitiesViewHolder activitiesViewHolder = new ActivitiesAdapter.ActivitiesViewHolder(view);
        return activitiesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ActivitiesViewHolder holder, final int position) {

        holder.label.setText(activityDTOS.get(position).getLabel());
        holder.start.setText("Start: "+ activityDTOS.get(position).getStart());
        holder.end.setText("End: "+ activityDTOS.get(position).getEnd());

        holder.behaviour.setText("Behaviour normal? "+ activityDTOS.get(position).getAnomalous());

        //adding recommendation
        holder.addRecommendation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                System.out.println("Add Recommendation button tapped");

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Add a recommendation");

                final EditText input = new EditText(context);

                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String recommendation = input.getText().toString().trim();
                        doctorController.addRecommendationToActivity(context, activityDTOS.get(position).getId(), recommendation);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        holder.changeStatus.setOnClickListener(new View.OnClickListener() {

            Boolean activityStatus = activityDTOS.get(position).getAnomalous();

            @Override
            public void onClick(View v) {
                System.out.println("Change activity status button tapped");
                activityStatus = !activityStatus;
                holder.behaviour.setText("Behaviour normal? "+ activityStatus.toString());
                doctorController.changeActivityAnomalousStatus(context, activityDTOS.get(position).getId(), activityStatus);
            }
        });

    }

    @Override
    public int getItemCount() {
        return activityDTOS.size();
    }

    public class ActivitiesViewHolder extends RecyclerView.ViewHolder {

        TextView label;
        TextView start;
        TextView end;
        TextView behaviour;
        Button addRecommendation;
        Button changeStatus;

        LinearLayout allActivitiesLayout;

        public ActivitiesViewHolder(@NonNull View itemView) {
            super(itemView);

            label = itemView.findViewById(R.id.sensor_activity_label);
            start = itemView.findViewById(R.id.sensor_activity_start);
            end = itemView.findViewById(R.id.sensor_activity_end);
            behaviour = itemView.findViewById(R.id.sensor_activity_behaviour);
            addRecommendation = itemView.findViewById(R.id.button_add_activity_recommendation);
            changeStatus = itemView.findViewById(R.id.button_change_activity_status);

            allActivitiesLayout = itemView.findViewById(R.id.allactivites_layout);
        }
    }

}
