package com.example.sd_android_project.views.fragments.medicationCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.medication.MedicationDTO;

public class CreateMedicationFragment extends Fragment {

    EditText name, sideEffects, dosage;
    Button addNewMedication;

    DoctorController doctorController = new DoctorController();

    public static CreateMedicationFragment newInstance() {

        Bundle args = new Bundle();

        CreateMedicationFragment fragment = new CreateMedicationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_medication, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        name = view.findViewById(R.id.new_med_name);
        sideEffects = view.findViewById(R.id.new_med_sideEffects);
        dosage = view.findViewById(R.id.new_med_dosage);
        addNewMedication = view.findViewById(R.id.add_medication);
        addNewMedication.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            createNewMedication(name.getText().toString().trim(),
                    sideEffects.getText().toString().trim(),
                    dosage.getText().toString().trim());
        }

    });
    }

    public void createNewMedication(String name, String sideEffects, String dosage) {
        int dosageInt = Integer.parseInt(dosage);

        MedicationDTO medicationDTO = new MedicationDTO(name, sideEffects, dosageInt);
        doctorController.createMedication(this.getContext(), medicationDTO);
    }


}
