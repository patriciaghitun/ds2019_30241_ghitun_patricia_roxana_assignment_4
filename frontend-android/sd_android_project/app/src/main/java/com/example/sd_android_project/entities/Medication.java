package com.example.sd_android_project.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Medication implements Serializable {

        @SerializedName("id")
        private Integer id;

        @SerializedName("name")
        private String name;

        @SerializedName("side_effects")
        private String side_effects;

        @SerializedName("dosage")
        private Integer dosage; // dozaj


        //Medication - MedicationPerPlan

        public Medication(){}

        public Medication(Integer id, String name, String side_effects, Integer dosage) {
            this.id = id;
            this.name = name;
            this.side_effects = side_effects;
            this.dosage = dosage;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSide_effects() {
            return side_effects;
        }

        public void setSide_effects(String side_effects) {
            this.side_effects = side_effects;
        }

        public Integer getDosage() {
            return dosage;
        }

        public void setDosage(Integer dosage) {
            this.dosage = dosage;
        }


    @Override
    public String toString() {
        return "Medication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", side_effects='" + side_effects + '\'' +
                ", dosage=" + dosage +
                '}';
    }
}