package com.example.sd_android_project.views.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.medication.MedicationDTO;
import com.example.sd_android_project.dto.medication.MedicationPerPlanDTO;
import com.example.sd_android_project.entities.Medication;
import com.example.sd_android_project.services.DoctorService;
import com.example.sd_android_project.views.adapters.MedStatusAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedStatusActivity extends AppCompatActivity {

    private static final String TAG = "MedStatusActivity";

    private Integer medPlanId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_med_plan_status_meds);
        System.out.println("ACTIVITY MED STATUS MEDS ");

        this.medPlanId = this.getIntent().getIntExtra("medicationPlanId", 0);
        System.out.println("MED PLAN ID IS: "+ this.medPlanId );

        //GET MEDICATIONS FOR THIS MED PLAN
        Call<List<MedicationPerPlanDTO>> medPerPlanCall =
                DoctorService.getInstance().getMedicationsForPlan(this.medPlanId);

        medPerPlanCall.enqueue(new Callback<List<MedicationPerPlanDTO>>() {
            @Override
            public void onResponse(Call<List<MedicationPerPlanDTO>> call, Response<List<MedicationPerPlanDTO>> response) {
                Log.d(TAG, "onResponse: getMedicationsForPlan");
                if(response.isSuccessful()) {
                    Log.d(TAG, "onResponse: successfull");

                    List<Medication> foundMeds = new ArrayList<>();
                    List<Boolean> medsTakenStatus = new ArrayList<>();
                    List<String> medsIntakes = new ArrayList<>();

                    for(MedicationPerPlanDTO medicationPerPlanDTO: response.body()) {
                        System.out.println("med per plan id: "+ medicationPerPlanDTO.getId());
                        System.out.println(medicationPerPlanDTO.getMedication().toString());
                        foundMeds.add(medicationPerPlanDTO.getMedication());

                        System.out.println("TAKEN SI INTAKE: " + medicationPerPlanDTO.getIntakemoment() + " - " + medicationPerPlanDTO.getTaken());
                        medsTakenStatus.add(medicationPerPlanDTO.getTaken());
                        medsIntakes.add(medicationPerPlanDTO.getIntakemoment());
                    }

                    List<MedicationDTO> foundMedsDTOs = new ArrayList<>();
                    for(Medication medication: foundMeds) {
                        MedicationDTO medicationDTO = new MedicationDTO(
                                medication.getId(),
                                medication.getName(),
                                medication.getSide_effects(),
                                medication.getDosage()
                        );
                        foundMedsDTOs.add(medicationDTO);
                    }

                    System.out.println("FOUND MEDS");
                    for(MedicationDTO medicationDTO: foundMedsDTOs) {
                        System.out.println(medicationDTO.toString());
                    }

                    RecyclerView recyclerView = findViewById(R.id.recycler_view_med_plans_status_meds);
                    MedStatusAdapter adapter = new MedStatusAdapter(foundMedsDTOs, getBaseContext(), medsTakenStatus, medsIntakes);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

                } else {
                    Log.d(TAG, "onResponse: failed");
                    Toast.makeText(MedStatusActivity.this, "Failed to retrieve meds for med plan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<MedicationPerPlanDTO>> call, Throwable t) {
                Log.d(TAG, "onFailure: getMedicationsForPlan");
                Toast.makeText(MedStatusActivity.this, "Failed to retrieve response from request", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}