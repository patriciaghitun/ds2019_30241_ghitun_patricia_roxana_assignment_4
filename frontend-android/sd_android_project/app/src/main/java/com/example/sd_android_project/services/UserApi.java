package com.example.sd_android_project.services;

import com.example.sd_android_project.dto.user.UserDTO;
import com.example.sd_android_project.dto.user.UserViewDTO;
import com.example.sd_android_project.entities.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserApi {

    @GET("/user/findById/{id}") //find user by id
    Call<UserViewDTO> getUserById(@Path("id") Integer id);

    @GET("/user/findByUsername/{username}")
    Call<UserDTO> getUserByUsername(@Path("username")String username);

    @GET("user/findUserById/{id}")
    Call<UserDTO> findUserById(@Path("id")Integer id);

    //CRUD ON USERS
    @GET("user/all")
    Call<List<UserDTO>> getAllUsers();


}
