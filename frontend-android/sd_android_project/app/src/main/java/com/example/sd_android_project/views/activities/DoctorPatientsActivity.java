package com.example.sd_android_project.views.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sd_android_project.views.fragments.activities.ActivityStatisticsFragment;
import com.example.sd_android_project.views.fragments.caregiversCRUD.CreateCaregiverFragment;
import com.example.sd_android_project.views.fragments.caregiversCRUD.DeleteCaregiverFragment;
import com.example.sd_android_project.views.fragments.caregiversCRUD.UpdateCaregiverFragment;
import com.example.sd_android_project.views.fragments.medicationCRUD.CreateMedicationFragment;
import com.example.sd_android_project.views.fragments.medicationCRUD.DeleteMedicationFragment;
import com.example.sd_android_project.views.fragments.medicationCRUD.UpdateMedicationFragment;
import com.example.sd_android_project.views.fragments.patientsCRUD.CreatePatientFragment;
import com.example.sd_android_project.views.fragments.caregiversCRUD.GetAllCaregiversFragment;
import com.example.sd_android_project.views.fragments.medicationCRUD.GetAllMedicationFragment;
import com.example.sd_android_project.views.fragments.patientsCRUD.DeletePatientFragment;
import com.example.sd_android_project.views.fragments.patientsCRUD.GetAllPatientsFragment;
import com.example.sd_android_project.R;
import com.example.sd_android_project.views.fragments.patientsCRUD.UpdatePatientFragment;

public class DoctorPatientsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //doctor ACTIVITY

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_patients);

        Bundle extras = getIntent().getExtras();
        System.out.println("BUNDLE:"+ extras.getString("message"));
        System.out.println("BUNDLE - USERNAME: "+extras.getString("username"));

        if (extras.getString("message")!= null) {

            //PATIENTS
            if ( extras.getString("message").equals("createPatient")) {
                System.out.println("BUNDLE CREATE PATIENT");

                String currentUsername = extras.getString("username");
                System.out.println("BUNDLE - CREATE PATIENT USERNAME 2:" + currentUsername);
                CreatePatientFragment createPatientFragment = new CreatePatientFragment();
                Bundle fragmentBundle = new Bundle();
                fragmentBundle.putString("username", currentUsername);

                createPatientFragment.setArguments(fragmentBundle);

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, createPatientFragment, "dogList")
                        .commit();
            }
            if ( extras.getString("message").equals("getAllPatients")) {
                System.out.println("BUNDLE GET ALL PATIENTS");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, GetAllPatientsFragment.newInstance(), "dogList")
                        .commit();
            }
            if ( extras.getString("message").equals("updatePatient")) {
                System.out.println("BUNDLE UPDATE PATIENT");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, UpdatePatientFragment.newInstance(), "dogList")
                        .commit();
            }
            if ( extras.getString("message").equals("deletePatient")) {
                System.out.println("BUNDLE DELETE PATIENT");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, DeletePatientFragment.newInstance(), "dogList")
                        .commit();
            }

            //CAREGIVERS
            if ( extras.getString("message").equals("createCaregiver")) {
                System.out.println("BUNDLE CREATE CAREGIVER");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, CreateCaregiverFragment.newInstance(), "dogList")
                        .commit();
            }
            if ( extras.getString("message").equals("getAllCaregivers")) {
                System.out.println("BUNDLE GET ALL CAREGIVERS");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, GetAllCaregiversFragment.newInstance(), "dogList")
                        .commit();
            }
            if ( extras.getString("message").equals("updateCaregiver")) {
                System.out.println("BUNDLE UPDATE CAREGIVER");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, UpdateCaregiverFragment.newInstance(), "dogList")
                        .commit();
            }
            if ( extras.getString("message").equals("deleteCaregiver")) {
                System.out.println("BUNDLE DELETE CAREGIVER");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, DeleteCaregiverFragment.newInstance(), "dogList")
                        .commit();
            }

            //MEDICATIONS
            if ( extras.getString("message").equals("createMedication")) {
                System.out.println("BUNDLE CREATE MEDICATION");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, CreateMedicationFragment.newInstance(), "dogList")
                        .commit();
            }

            if ( extras.getString("message").equals("getAllMedication")) {
                System.out.println("BUNDLE GET ALL MEDICATION");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, GetAllMedicationFragment.newInstance(), "dogList")
                        .commit();
            }
            if ( extras.getString("message").equals("updateMedication")) {
                System.out.println("BUNDLE UPDATE MEDICATION");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, UpdateMedicationFragment.newInstance(), "dogList")
                        .commit();
            }
            if ( extras.getString("message").equals("deleteMedication")) {
                System.out.println("BUNDLE DELETE MEDICATION");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, DeleteMedicationFragment.newInstance(), "dogList")
                        .commit();
            }

            //MEDICATION PLAN
            if ( extras.getString("message").equals("createMedicationPlan")) {
                String currentUsername = extras.getString("username");

                System.out.println("BUNDLE CREATE MEDICATION PLAN");

                Intent intent = new Intent(DoctorPatientsActivity.this, AddMedicationPlan.class);
                intent.putExtra("username", currentUsername); // username-ul logat
                DoctorPatientsActivity.this.startActivity(intent);


//                CreateMedicationPlanFragment createMedicationPlanFragment = new CreateMedicationPlanFragment();
//                Bundle fragmentBundle = new Bundle();
//                fragmentBundle.putString("username", currentUsername);
//
//                createMedicationPlanFragment.setArguments(fragmentBundle);
//
//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.doctor_patient_frame, createMedicationPlanFragment, "dogList")
//                        .commit();
            }

            if ( extras.getString("message").equals("showActivitiesStatistics")) {
                System.out.println("BUNDLE SHOW ACTIVITIES STATISTICS");
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.doctor_patient_frame, ActivityStatisticsFragment.newInstance(), "dogList")
                        .commit();
            }

        }
    }
}