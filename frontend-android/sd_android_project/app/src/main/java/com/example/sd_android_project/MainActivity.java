package com.example.sd_android_project;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sd_android_project.controller.UserController;

public class MainActivity extends AppCompatActivity {

    UserController userController = new UserController();
    private EditText usernameEditText, passwordEditText ;
    private Button loginButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //LOGIN ACTIVITY

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usernameEditText = findViewById(R.id.login_username);
        passwordEditText = findViewById(R.id.login_password);
        loginButton = findViewById(R.id.login_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameEditText.getText().toString().trim();
                String password = passwordEditText.getText().toString().trim();
                System.out.println("Credentials:"+ username +" " + password);

                userController.login(getBaseContext(),username, password);
            }
        });

    }

}
