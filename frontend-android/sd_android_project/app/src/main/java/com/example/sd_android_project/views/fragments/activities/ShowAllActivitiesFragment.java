package com.example.sd_android_project.views.fragments.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.user.ActivityDTO;
import com.example.sd_android_project.services.DoctorService;
import com.example.sd_android_project.views.adapters.ActivitiesAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowAllActivitiesFragment extends Fragment {

    private static final String TAG = "ShowAllActivitiesFragme";
    private Integer patientId;

    public static ShowAllActivitiesFragment newInstance() {

        Bundle args = new Bundle();

        ShowAllActivitiesFragment fragment = new ShowAllActivitiesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_show_all_activities, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        System.out.println("Show all activities fragment");

        this.patientId = getArguments().getInt("patientId");

        final Call<List<ActivityDTO>> activities = DoctorService.getInstance().getActivitiesForPatientWithSOAP(patientId);
        activities.enqueue(new Callback<List<ActivityDTO>>() {
            @Override
            public void onResponse(Call<List<ActivityDTO>> call, Response<List<ActivityDTO>> response) {
                Log.d(TAG, "onResponse: ");
                if(response.isSuccessful()) {
                    Log.d(TAG, "onResponse: successful :" + response.body());

                    List<ActivityDTO> activityDTOS = new ArrayList<>();

                    for (ActivityDTO activityDTO : response.body()) {
                        activityDTOS.add(activityDTO);
                    }

                    RecyclerView recyclerView = view.findViewById(R.id.recycler_view_activities);
                    ActivitiesAdapter adapter = new ActivitiesAdapter(activityDTOS, view.getContext());

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

                } else {
                    Log.d(TAG, "onResponse: failed");
                    Toast.makeText(getContext(), "Nu exista activitati pentru pacientul introdus!", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<List<ActivityDTO>> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                Toast.makeText(getContext(), "Nu s-au putut lua activitatile pentru pacientul introdus!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
