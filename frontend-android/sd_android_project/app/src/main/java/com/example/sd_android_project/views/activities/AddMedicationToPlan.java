package com.example.sd_android_project.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.builders.MedicationBuilder;
import com.example.sd_android_project.dto.builders.MedicationPlanBuilder;
import com.example.sd_android_project.dto.medication.MedicationDTO;
import com.example.sd_android_project.dto.medication.MedicationPerPlanDTO;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.services.DoctorService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMedicationToPlan extends AppCompatActivity {
    private static final String TAG = "AddMedicationToPlan";

    EditText medId, medName, medSideEEffetcs, medDosage, intakeMomentET;
    Button addMedButton;

    DoctorController doctorController = new DoctorController();
    Integer medPlanId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medication_to_plan);

        medPlanId = getIntent().getIntExtra("medicationPlanId",0);

        System.out.println("MEDICATION PLAN ID:"+medPlanId);

        medId = findViewById(R.id.new_med_id);
        medName = findViewById(R.id.new_med_name);
        medSideEEffetcs = findViewById(R.id.new_med_sideEffects);
        medDosage = findViewById(R.id.new_med_dosage);
        intakeMomentET = findViewById(R.id.new_med_intake);

        addMedButton = findViewById(R.id.new_med_add_button);
        addMedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: add medication button tapped");
                final MedicationDTO medicationDTO = new MedicationDTO(
                        Integer.parseInt(medId.getText().toString().trim()),
                        medName.getText().toString().trim(),
                        medSideEEffetcs.getText().toString().trim(),
                        Integer.parseInt(medDosage.getText().toString().trim())
                );
                final String intakeMoment = intakeMomentET.getText().toString().trim();
                //doctorController.addMedicationToPlan(AddMedicationToPlan.this, medPlanId, medicationDTO, intakeMoment);


                                Call<MedicationPlanDTO> medicationPlanDTOCall = DoctorService.getInstance().getMedicationPlanById(medPlanId);
                                medicationPlanDTOCall.enqueue(new Callback<MedicationPlanDTO>() {
                                    @Override
                                    public void onResponse(Call<MedicationPlanDTO> call, Response<MedicationPlanDTO> response) {
                                        Log.d(TAG, "onResponse: addMedicationToPlan - getMedPlan");
                                        if(response.isSuccessful()) {
                                            Log.d(TAG, "onResponse: addMedicationToPlan - getMedPlan successful ");

                                            MedicationPlanDTO medicationPlanDTO = response.body();
                                            //create a med per plan dto
                                            MedicationPerPlanDTO medicationPerPlanDTO = new MedicationPerPlanDTO(
                                                    intakeMoment,
                                                    MedicationBuilder.generateEntityFromDTO(medicationDTO),
                                                    MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)
                                            );

                                            //add the medperplan dto = adding a medication to a plan
                                            Call<Integer> addMedToPlanCall = DoctorService.getInstance().addMedicationToPlan(medicationPerPlanDTO);
                                            addMedToPlanCall.enqueue(new Callback<Integer>() {
                                                @Override
                                                public void onResponse(Call<Integer> call, Response<Integer> response) {
                                                    Log.d(TAG, "onResponse: addMedToPlanCall");
                                                    if (response.isSuccessful()) {
                                                        Log.d(TAG, "onResponse: addMedToPlanCall - successful");
                                                        Toast.makeText(AddMedicationToPlan.this, "Add medication to plan - successful", Toast.LENGTH_SHORT).show();

                                                        Intent resultIntent = new Intent(AddMedicationToPlan.this, AddMedicationPlan.class);
                                                        setResult(RESULT_OK, resultIntent);
                                                        finish();

                                                    } else {
                                                        Log.d(TAG, "onResponse: addMedToPlanCall - failed");
                                                        Toast.makeText(AddMedicationToPlan.this, "Add medication to plan - failed", Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<Integer> call, Throwable t) {
                                                    Log.d(TAG, "onFailure: addMedToPlanCall");
                                                }
                                            });

                                        } else {
                                            Log.d(TAG, "onResponse: addMedicationToPlan - getMedPlan failed");
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<MedicationPlanDTO> call, Throwable t) {
                                        Log.d(TAG, "onFailure: addMedicationToPlan - getMedPlan");
                                    }
                                });

                            }
                        });
    }

}
