package com.example.sd_android_project.views.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.services.DoctorService;
import com.example.sd_android_project.services.PatientService;
import com.example.sd_android_project.views.adapters.CaregiverPatientsAdapter;
import com.example.sd_android_project.views.adapters.MPMedicationAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CaregiverActivity extends AppCompatActivity {

    private static final String TAG = "CaregiverActivity";

    protected void onCreate(Bundle savedInstanceState) {
        //doctor ACTIVITY
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caregiver);

        Bundle extras = getIntent().getExtras();
        final String currentUsername = extras.getString("username");
        System.out.println("CURRENT USERNAME : "+ currentUsername);

        getAllPatients(currentUsername);



    }


    public void getAllPatients(String currentUsername) {
        Call<List<PatientDTO>> patientsList = PatientService.getInstance().getAllPatientsByCaregiverUsername(currentUsername);
        patientsList.enqueue(new Callback<List<PatientDTO>>() {
            @Override
            public void onResponse(Call<List<PatientDTO>> call, Response<List<PatientDTO>> response) {
                Log.d(TAG, "onResponse: getAllPatients ");
                if(response.isSuccessful()) {
                    Log.d(TAG, "onResponse: getAllPatients - successful");
                    RecyclerView recyclerView = findViewById(R.id.caregiver_all_patients_recycler_view);
                    CaregiverPatientsAdapter adapter = new CaregiverPatientsAdapter(response.body(), CaregiverActivity.this);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(CaregiverActivity.this));
                } else {
                    Log.d(TAG, "onResponse: getAllPatients - failed");
                    Toast.makeText(CaregiverActivity.this,"The caregiver has no patients", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<PatientDTO>> call, Throwable t) {
                Log.d(TAG, "onFailure: getAllPatients");
            }
        });
    }
}
