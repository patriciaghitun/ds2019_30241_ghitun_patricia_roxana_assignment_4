package com.example.sd_android_project.views.fragments.caregiversCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.user.UserDTO;
import com.example.sd_android_project.entities.Role;


public class CreateCaregiverFragment extends Fragment {

    EditText username, password, name, birthDate, gender, address;
    Button addNewCaregiver;

    DoctorController doctorController = new DoctorController();

    public static CreateCaregiverFragment newInstance() {

        Bundle args = new Bundle();

        CreateCaregiverFragment fragment = new CreateCaregiverFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_caregiver, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //get caregiver data
        username = view.findViewById(R.id.new_caregiver_username);
        password = view.findViewById(R.id.new_caregivert_password);

        name = view.findViewById(R.id.new_caregiver_name);
        birthDate = view.findViewById(R.id.new_caregiver_birthDate);
        gender = view.findViewById(R.id.new_caregiver_gender);
        address = view.findViewById(R.id.new_caregiver_address);

        addNewCaregiver = view.findViewById(R.id.new_caregiver_button);
        addNewCaregiver.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                System.out.println("add new caregiver button tapped");

                UserDTO caregiverDTO = new UserDTO(
                                                    username.getText().toString().trim(),
                                                    password.getText().toString().trim(),
                                                    name.getText().toString().trim(),
                                                    birthDate.getText().toString().trim(),
                                                    gender.getText().toString().trim(),
                                                    address.getText().toString().trim(),
                                                    Role.CAREGIVER);

                createNewMedication(caregiverDTO);
            }

        });
    }

    public void createNewMedication(UserDTO caregiverDTO) {
        doctorController.addNewCaregiver(this.getActivity().getBaseContext(), caregiverDTO);
    }
}
