package com.example.sd_android_project.views.fragments.patientsCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.user.UserDTO;
import com.example.sd_android_project.entities.Role;

import java.util.Currency;

public class CreatePatientFragment extends Fragment {

    EditText username, password, name, birthDate, gender, address, caregiver, medicalRecord;

    Button addPatientButton;

    DoctorController doctorController = new DoctorController();

    public static CreatePatientFragment newInstance() {
        
        Bundle args = new Bundle();
        
        CreatePatientFragment fragment = new CreatePatientFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_patient, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final String currentUsername = getArguments().getString("username");

        System.out.println("CREATE PATIENT FRAGMENT: - CURRENT USERNAME " + currentUsername);
        username = view.findViewById(R.id.new_patient_username);
        password = view.findViewById(R.id.new_patient_password);
        name = view.findViewById(R.id.new_patient_name);
        birthDate = view.findViewById(R.id.new_patient_birthDate);
        gender = view.findViewById(R.id.new_patient_gender);
        address = view.findViewById(R.id.new_patient_address);

        caregiver = view.findViewById(R.id.new_patient_caregiver_id);
        medicalRecord = view.findViewById(R.id.new_patient_medical_record);

        addPatientButton = view.findViewById(R.id.new_patient_button);
        addPatientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer caregiverId = Integer.parseInt(caregiver.getText().toString().trim());

                //datele despre userul pacient
                UserDTO userDTO = new UserDTO(
                        username.getText().toString().trim(),
                        password.getText().toString().trim(),
                        name.getText().toString().trim(),
                        birthDate.getText().toString().trim(),
                        gender.getText().toString().trim(),
                        address.getText().toString().trim(),
                        Role.PATIENT);
                //medical record

                //datele despre caregiver vor fi luate cu getUserDTO by id

                addNewPatient(userDTO,medicalRecord.getText().toString().trim(),caregiverId,currentUsername);
            }
        });
    }

    private void addNewPatient(UserDTO newUserDTO, String medicalRecord, Integer caregiverId, String doctorUsername) {
        doctorController.addNewPatient(this.getContext(), newUserDTO, medicalRecord, caregiverId, doctorUsername);
    }

}
