package com.example.sd_android_project.dto.builders;

import com.example.sd_android_project.dto.user.ActivityDTO;
import com.example.sd_android_project.entities.ActivityEntity;

public class ActivityBuilder {
    private ActivityBuilder() {
    }

    public static ActivityDTO generateDTOFromEntity(ActivityEntity activity) {
        return new ActivityDTO(
                activity.getId(),
                activity.getStart(),
                activity.getEnd(),
                activity.getLabel(),
                activity.getPatientId(),
                activity.getAnomalous(),
                activity.getRecommendation());
    }

    public static ActivityEntity generateEntityFromDTO(ActivityDTO activityDTO) {
        return new ActivityEntity(
                activityDTO.getId(),
                activityDTO.getStart(),
                activityDTO.getEnd(),
                activityDTO.getLabel(),
                activityDTO.getPatientId(),
                activityDTO.getAnomalous(),
                activityDTO.getRecommendation()
        );
    }
}
