package com.example.sd_android_project.dto.medication;

public class MedicationDTO {
    private Integer id;
    private String name;
    private String side_effects;
    private Integer dosage;

    public MedicationDTO() {}

    public MedicationDTO(String name, String side_effects, Integer dosage) {
        this.name = name;
        this.side_effects = side_effects;
        this.dosage = dosage;
    }

    public MedicationDTO(Integer id, String name, String side_effects, Integer dosage) {
        this.id = id;
        this.name = name;
        this.side_effects = side_effects;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide_effects() {
        return side_effects;
    }

    public void setSide_effects(String side_effects) {
        this.side_effects = side_effects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return "MedicationDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", side_effects='" + side_effects + '\'' +
                ", dosage=" + dosage +
                '}';
    }
}
