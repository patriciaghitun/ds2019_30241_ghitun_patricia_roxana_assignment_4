package com.example.sd_android_project.views.fragments.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.services.DoctorService;
import com.example.sd_android_project.views.adapters.MedPlanStatusAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedPlanStatusFragment extends Fragment {

    private static final String TAG = "MedPlanStatusFragment";
    private Integer patientId;
    private String day;

    public static MedPlanStatusFragment newInstance() {

        Bundle args = new Bundle();

        MedPlanStatusFragment fragment = new MedPlanStatusFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_med_plan_status, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        System.out.println("med plan staus fragment");


        this.patientId = getArguments().getInt("patientId");
        this.day = getArguments().getString("day");

        final Call<List<MedicationPlanDTO>> medicationPlans =
                DoctorService.getInstance().getMedPlansForPatientWithSOAP(patientId);

        medicationPlans.enqueue(new Callback<List<MedicationPlanDTO>>() {
            @Override
            public void onResponse(Call<List<MedicationPlanDTO>> call, Response<List<MedicationPlanDTO>> response) {
                Log.d(TAG, "onResponse: getMedPlansForPatient");
                if(response.isSuccessful()) {
                    List<MedicationPlanDTO> foundMedPlans = new ArrayList<>();
                    for(MedicationPlanDTO medicationPlanDTO: response.body()) {

                        if(medicationPlanDTO.getStart().equals(day)) {
                            //doar pe cele date in ziua corespunzatoare
                            foundMedPlans.add(medicationPlanDTO);
                        }
                    }

                    RecyclerView recyclerView = view.findViewById(R.id.recycler_view_med_plans_status);
                    MedPlanStatusAdapter adapter = new MedPlanStatusAdapter(foundMedPlans, view.getContext());

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

                } else {
                    Toast.makeText(getContext(), "Nu exista med plans pt pacientul dat",Toast.LENGTH_SHORT ).show();
                }
            }

            @Override
            public void onFailure(Call<List<MedicationPlanDTO>> call, Throwable t) {
                Log.d(TAG, "onFailure: getMedPlansForPatient");
                Toast.makeText(getContext(), "Could not retrieve med plans - onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
