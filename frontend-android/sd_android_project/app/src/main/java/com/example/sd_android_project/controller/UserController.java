package com.example.sd_android_project.controller;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sd_android_project.views.activities.CaregiverActivity;
import com.example.sd_android_project.views.activities.DoctorActivity;
import com.example.sd_android_project.dto.user.UserDTO;
import com.example.sd_android_project.dto.user.UserViewDTO;
import com.example.sd_android_project.entities.Role;
import com.example.sd_android_project.services.UserService;
import com.example.sd_android_project.views.activities.PatientActivity;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserController {

    //get user by id - user view dto
    public void getUserById(final Context context, Integer userId) {

        Call<UserViewDTO> user = UserService.getInstance().getUserById(userId);

        user.enqueue(new Callback<UserViewDTO>() {

            @Override
            public void onResponse(Call<UserViewDTO> call, Response<UserViewDTO> response) {
                if (response.isSuccessful()) {
                    Log.d("Get user by id", "Success");
                    Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                    System.out.println("userul:"+response.body().getUsername());

                } else {
                    Log.d("Get user by id", "Fail");
                    Toast.makeText(context, "Get user by id - fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserViewDTO> call, Throwable t) {
                Log.d("Get user by id", "onFailure");
            }
        });
    }

    public void getUserByUsername(final Context context, String username) {

        Call<UserDTO> user = UserService.getInstance().getUserByUsername(username);
        user.enqueue(new Callback<UserDTO>() {

            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    Log.d("Get user by username", "Success");
                    Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                    System.out.println("userul:"+response.body().getUsername());
                } else {
                    Log.d("Get user by username", "Fail");
                    Toast.makeText(context, "Get user by id - fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.d("Get user by username", "onFailure");
            }
        });
    }

    public void login(final Context context, final String username, final String password) {

        Call<UserDTO> loggedUser = UserService.getInstance().getUserByUsername(username);
        loggedUser.enqueue(new Callback<UserDTO>() {

            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    if (response.body().getPassword().equals(password)) {
                        Toast.makeText(context, "Login successful", Toast.LENGTH_SHORT).show();
                        //check for user role:
                        if (response.body().getRole() == Role.DOCTOR) {
                            Intent intent = new Intent(context, DoctorActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("username", username);
                            context.startActivity(intent);
                        }
                        if (response.body().getRole() == Role.CAREGIVER) {
                            Intent intent = new Intent(context, CaregiverActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("username", username);
                            context.startActivity(intent);
                        }
                        if (response.body().getRole() == Role.PATIENT) {
                            Intent intent = new Intent(context, PatientActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("username", username);
                            context.startActivity(intent);
                        }

                    } else {
                        Toast.makeText(context, "Wrong password", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Login failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.d("Login call - failed", "onFailure");
                Toast.makeText(context, "Connection refused! Check BASE_URL/ server", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getAllUsers(final Context context, ListView usersListView) {

        Call<List<UserDTO>> users = UserService.getInstance().getAllUsers();

        users.enqueue(new Callback<List<UserDTO>>() {
            @Override
            public void onResponse(Call<List<UserDTO>> call, final Response<List<UserDTO>> response) {
                if (response.isSuccessful()) {
                    Log.d("Get all users", "Success");
                    //afisez doar numele in list view
                    List<String> usernames = new ArrayList<String>();
                    for(UserDTO userDTO: response.body()) {
                        usernames.add(userDTO.getUsername());
                    }

//                        List<String> recipeNames=new ArrayList<>();
//                        for(Recipe recipe : response.body()){
//                            recipeNames.add(recipe.getName());
//                            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
//                                    context,
//                                    android.R.layout.simple_list_item_1,
//                                    recipeNames );
//
//                            listView.setAdapter(arrayAdapter);
//                        }

                } else {
                    Log.d("Get all users", "Fail");
                    Toast.makeText(context, "Get user by id - fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<UserDTO>> call, Throwable t) {
                Log.d("Get all users", "onFailure");
            }
        });

    }
}
