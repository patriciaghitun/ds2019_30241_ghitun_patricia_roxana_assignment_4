package com.example.sd_android_project.views.fragments.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;


public class ActivityStatisticsFragment extends Fragment {

    private static final String TAG = "ActivityStatisticsFragm";

    //1. For showing activities from a certain patient and a certain day as a chart
    private Button showActivities;
    private EditText patientIdEt, dayEt;

    //2. Showing activities and their patients' behaviours according to them
    private Button showPatientBehaviours;
    private EditText forBehaviourPatientId;

    //3. Show medication from med plans - taken / not
    private EditText patientIdForMedPlan, dayForMedPlan;
    private Button showMedPlansForPatient;

    public static ActivityStatisticsFragment newInstance() {

        Bundle args = new Bundle();

        ActivityStatisticsFragment fragment = new ActivityStatisticsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_show_activiy_statistics, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //1.
        patientIdEt = view.findViewById(R.id.activity_patient_id);
        dayEt = view.findViewById(R.id.activity_day);

        showActivities = view.findViewById(R.id.button_show_activites);
        showActivities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer patientId = Integer.parseInt(patientIdEt.getText().toString().trim());
                String day = dayEt.getText().toString().trim();

                Bundle bundle = new Bundle();
                bundle.putInt("patientId", patientId);
                bundle.putString("day", day);

                ActivityChartFragment activityChartFragment = new ActivityChartFragment();
                activityChartFragment.setArguments(bundle);

                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.doctor_patient_frame, activityChartFragment)
                        .addToBackStack("dogList")
                        .commit();
            }
        });

        //2.
        forBehaviourPatientId = view.findViewById(R.id.sensor_data_patient_id);
        showPatientBehaviours = view.findViewById(R.id.button_check_patient_anomalous);

        showPatientBehaviours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Integer patientId = Integer.parseInt(forBehaviourPatientId.getText().toString().trim());

                Bundle bundle = new Bundle();
                bundle.putInt("patientId", patientId);

                ShowAllActivitiesFragment showAllActivitiesFragment = new ShowAllActivitiesFragment();
                showAllActivitiesFragment.setArguments(bundle);

                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.doctor_patient_frame, showAllActivitiesFragment)
                        .addToBackStack("dogList")
                        .commit();
            }
        });

        //3.
        patientIdForMedPlan = view.findViewById(R.id.med_plan_patient);
        dayForMedPlan = view.findViewById(R.id.med_plan_day);
        showMedPlansForPatient = view.findViewById(R.id.button_med_plan_status);

        showMedPlansForPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer patientId =  Integer.parseInt(patientIdForMedPlan.getText().toString().trim());
                String medPlanDay =  dayForMedPlan.getText().toString().trim();

                Bundle bundle = new Bundle();
                bundle.putInt("patientId", patientId);
                bundle.putString("day", medPlanDay);

                MedPlanStatusFragment medPlanStatusFragment = new MedPlanStatusFragment();
                medPlanStatusFragment.setArguments(bundle);

                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.doctor_patient_frame, medPlanStatusFragment)
                        .addToBackStack("dogList")
                        .commit();
            }
        });
    }

}
