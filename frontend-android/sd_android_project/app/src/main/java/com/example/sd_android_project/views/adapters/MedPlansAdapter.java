package com.example.sd_android_project.views.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.medication.MedicationDTO;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.entities.MedicationPerPlan;

import java.util.ArrayList;
import java.util.List;

public class MedPlansAdapter extends RecyclerView.Adapter<MedPlansAdapter.MedPlanViewHolder>{

    private static final String TAG = "MedPlansAdapter";

    private List<MedicationPlanDTO> medicationPlansDTOs = new ArrayList<>();
    private Context context ;


    public MedPlansAdapter(List<MedicationPlanDTO> medicationPlansDTOs, Context context) {
        this.medicationPlansDTOs = medicationPlansDTOs;
        this.context = context;
    }

    @NonNull
    @Override
    public MedPlanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.allmedplans_recycler_item, parent, false);
        MedPlansAdapter.MedPlanViewHolder medPlanViewHolder = new MedPlansAdapter.MedPlanViewHolder(view);
        return medPlanViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull MedPlanViewHolder holder, int position) {
        holder.medPlanId.setText(medicationPlansDTOs.get(position).getId()+"");
        holder.start.setText(medicationPlansDTOs.get(position).getStart());
        holder.end.setText(medicationPlansDTOs.get(position).getEnd());
    }

    @Override
    public int getItemCount() {
        return medicationPlansDTOs.size();
    }


    public class MedPlanViewHolder extends RecyclerView.ViewHolder {

        TextView medPlanId;
        TextView start;
        TextView end;
        LinearLayout allMedPlansLayout;

        public MedPlanViewHolder(@NonNull View itemView) {
            super(itemView);

            medPlanId = itemView.findViewById(R.id.all_medplans_plan_id);
            start = itemView.findViewById(R.id.plan_start);
            end = itemView.findViewById(R.id.plan_end);
            allMedPlansLayout = itemView.findViewById(R.id.allmedplans_layout);
        }
    }
}