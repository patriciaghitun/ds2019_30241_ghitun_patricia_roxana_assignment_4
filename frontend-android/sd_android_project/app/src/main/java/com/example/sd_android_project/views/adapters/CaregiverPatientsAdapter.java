package com.example.sd_android_project.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.dto.user.UserDTO;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class CaregiverPatientsAdapter extends RecyclerView.Adapter<CaregiverPatientsAdapter.CaregiverPatientViewHolder>{

    private static final String TAG = "CaregiverPatientsAdapter";

    private List<PatientDTO> patientDTOS = new ArrayList<>();
    private Context context ;

    public CaregiverPatientsAdapter(List<PatientDTO> patientDTOS, Context context) {
        this.patientDTOS = patientDTOS;
        this.context = context;
    }

    @NonNull
    @Override
    public CaregiverPatientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.allpatients_caregiver_recycler_item, parent, false);

        CaregiverPatientsAdapter.CaregiverPatientViewHolder caregiverPatientViewHolder = new CaregiverPatientsAdapter.CaregiverPatientViewHolder(view);
        return caregiverPatientViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CaregiverPatientViewHolder holder, int position) {
        holder.id.setText(patientDTOS.get(position).getId()+"");
        holder.name.setText(patientDTOS.get(position).getUser().getName());
        holder.username.setText(patientDTOS.get(position).getUser().getUsername());
        holder.medRecord.setText(patientDTOS.get(position).getMedicalRecord());
    }

    @Override
    public int getItemCount() {
        return patientDTOS.size();
    }


    public class CaregiverPatientViewHolder extends RecyclerView.ViewHolder {

        TextView id;
        TextView name;
        TextView username;
        TextView medRecord;

        LinearLayout allCaregiverPatientsLayout;

        public CaregiverPatientViewHolder(@NonNull View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.caregiver_patient_id);
            name = itemView.findViewById(R.id.caregiver_patient_name);
            username = itemView.findViewById(R.id.caregiver_patient_username);
            medRecord = itemView.findViewById(R.id.caregiver_patient_med_record);
            allCaregiverPatientsLayout = itemView.findViewById(R.id.caregiver_patient_layout);
        }
    }
}