package com.example.sd_android_project.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.medication.MedicationPerPlanDTO;

import java.util.ArrayList;
import java.util.List;

public class MPMedicationAdapter extends RecyclerView.Adapter<MPMedicationAdapter.MPMedicationViewHolder> {
    private static final String TAG = "MPMedicationAdapter";

    private List<MedicationPerPlanDTO> medications = new ArrayList<>();
    private Context context ;

    public MPMedicationAdapter(List<MedicationPerPlanDTO> medications, Context context) {
        this.medications = medications;
        this.context = context;
    }

    @NonNull
    @Override
    public MPMedicationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mp_medication_recycler_item, parent, false);
        MPMedicationAdapter.MPMedicationViewHolder mpMedicationViewHolder = new MPMedicationAdapter.MPMedicationViewHolder(view);
        return mpMedicationViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MPMedicationViewHolder holder, int position) {

        holder.medName.setText(medications.get(position).getMedication().getName());
        holder.medSideEffects.setText(medications.get(position).getMedication().getSide_effects());
        holder.intakeMoment.setText(medications.get(position).getIntakemoment());
    }

    @Override
    public int getItemCount() {
        return medications.size();
    }

    //dedicated view holder
    public class MPMedicationViewHolder extends RecyclerView.ViewHolder {

        TextView medName;
        TextView medSideEffects;
        TextView intakeMoment;
        LinearLayout mpRecyclerLayout;

        public MPMedicationViewHolder(@NonNull View itemView) {
            super(itemView);

            medName = itemView.findViewById(R.id.mp_name);
            medSideEffects = itemView.findViewById(R.id.mp_side_effects);
            intakeMoment = itemView.findViewById(R.id.mp_intake_moment);

            mpRecyclerLayout = itemView.findViewById(R.id.mp_recyler_item_layout);
        }
    }

}
