package com.example.sd_android_project.services;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PatientService {

    private final static String API_URL = "http://192.168.43.82:8080/";

    private static PatientApi patientApi;

    //get la instanta de retrofit

    public static PatientApi getInstance() {
        if (patientApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttp())
                    .build();

            patientApi = retrofit.create(PatientApi.class);
        }
        return patientApi;
    }

    private static OkHttpClient provideOkHttp() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        return httpBuilder.build();
    }

}
