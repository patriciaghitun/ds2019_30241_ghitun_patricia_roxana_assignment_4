package com.example.sd_android_project.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.PatientController;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.services.PatientService;
import com.example.sd_android_project.views.adapters.MedPlansAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientMedPlansActivity extends AppCompatActivity {
    private static final String TAG = "PatientMedPlansActivity";

    Integer patientId;
    PatientController patientController = new PatientController();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_medplans);

        patientId = getIntent().getIntExtra("patientId",0);
        System.out.println("PATIENT ID : "+patientId);

        getPatientMedicationPlans(patientId);
    }

    public void getPatientMedicationPlans(Integer id) {
        Call<List<MedicationPlanDTO>> medList = PatientService.getInstance().getAllByPatientId(patientId);
        medList.enqueue(new Callback<List<MedicationPlanDTO>>() {
            @Override
            public void onResponse(Call<List<MedicationPlanDTO>> call, Response<List<MedicationPlanDTO>> response) {


                if(response.isSuccessful()) {
                    System.out.println("RESPONSE SUCCESSFUL");
                    Log.d(TAG, "onResponse: successful - showing med plans for patient id" + patientId);

//                    for(MedicationPlanDTO medicationPlanDTO: response.body()) {
//                        Log.d(TAG, "onResponse: med plan:"+ medicationPlanDTO.getId());
//                        System.out.println("Medication plan id:" + medicationPlanDTO.getId());
//                    }

                    RecyclerView recyclerView = findViewById(R.id.recycler_view_med_plans);
                    MedPlansAdapter adapter = new MedPlansAdapter(response.body(), PatientMedPlansActivity.this);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(PatientMedPlansActivity.this));

                } else {
                    System.out.println("RESPONSE FAILED");
                    Toast.makeText(PatientMedPlansActivity.this, "Could not get any med plans!", Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<List<MedicationPlanDTO>> call, Throwable t) {

            }
        });

    }

}
