package com.example.sd_android_project.entities;

import com.google.gson.annotations.SerializedName;

public class MedicationPerPlan {

    @SerializedName("id")
    private Integer id;

    @SerializedName("intakemoment")
    private String intakemoment;

    @SerializedName("medication_id")
    private Medication medication;

    @SerializedName("medication_plan_id")
    private MedicationPlan medicationPlan;

    @SerializedName("taken")
    private Boolean taken;

    public MedicationPerPlan(Integer id, String intakemoment, Medication medication, MedicationPlan medicationPlan, Boolean taken) {
        this.id = id;
        this.intakemoment = intakemoment;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
        this.taken = taken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntakemoment() {
        return intakemoment;
    }

    public void setIntakemoment(String intakemoment) {
        this.intakemoment = intakemoment;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public Boolean getTaken() {
        return taken;
    }

    public void setTaken(Boolean taken) {
        this.taken = taken;
    }

    @Override
    public String toString() {
        return "MedicationPerPlan{" +
                "id=" + id +
                ", intakemoment='" + intakemoment + '\'' +
                ", medication=" + medication +
                ", medicationPlan=" + medicationPlan +
                ", taken=" + taken +
                '}';
    }
}
