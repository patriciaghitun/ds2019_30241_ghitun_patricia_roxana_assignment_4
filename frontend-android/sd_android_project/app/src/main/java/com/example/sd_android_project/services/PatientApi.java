package com.example.sd_android_project.services;

import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.entities.MedicationPlan;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PatientApi {

    @GET("patient/findByUsername/{username}")
    Call<PatientDTO> findPatientByUsername(@Path("username") String username);

    @GET("patient/findById/{id}")
    Call<PatientDTO> findPatientById(@Path("id") Integer id);

    @GET("patient/getAllByPatientId/{id}")
    Call<List<MedicationPlanDTO>> getAllByPatientId(@Path("id") Integer id);

    @GET("patient/getAllPatientsByCaregiverUsername/{username}")
    Call<List<PatientDTO>> getAllPatientsByCaregiverUsername(@Path("username")String username);

}
