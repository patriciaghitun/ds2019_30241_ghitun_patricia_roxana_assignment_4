package com.example.sd_android_project.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.builders.PatientBuilder;
import com.example.sd_android_project.dto.medication.MedicationPerPlanDTO;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.services.DoctorService;
import com.example.sd_android_project.views.adapters.MPMedicationAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMedicationPlan extends AppCompatActivity {

    private static final String TAG = "AddMedicationPlan";
    EditText patientId, startTime, endTime;
    Button save, addMedication;

    Bundle bundle = new Bundle();
    Integer id;

    //private RecyclerView recyclerView;
    //private MPMedicationAdapter mpMedicationAdapter;
    private List<MedicationPerPlanDTO> medications = new ArrayList<>();

    DoctorController doctorController = new DoctorController();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_med_plan);

        patientId = this.findViewById(R.id.mp_patient_id);
        startTime = this.findViewById(R.id.mp_start_time);
        endTime = this.findViewById(R.id.mp_end_time);

        addMedication = this.findViewById(R.id.add_medication_to_plan);
        save = this.findViewById(R.id.save_medication_plan);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               getNewMedicationPlanId(Integer.parseInt(patientId.getText().toString().trim()),
                                                            startTime.getText().toString().trim(),
                                                            endTime.getText().toString().trim());
            }
        });

        addMedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: add medication");
                goToAddMedToPlan();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getAllMedicationsFromPlan();
    }

    public void getNewMedicationPlanId(Integer patientId, final String startTime, final String endTime) {

        Call<PatientDTO> patientDTOCall = DoctorService.getInstance().findPatientById(patientId);
        patientDTOCall.enqueue(new Callback<PatientDTO>() {
            @Override
            public void onResponse(Call<PatientDTO> call, Response<PatientDTO> response) {
                if(response.isSuccessful()) {
                    Log.d("Find patient by id!", "onSuccess");

                    PatientDTO currentPatient = response.body();
                    MedicationPlanDTO newMedPlan = new MedicationPlanDTO(
                            PatientBuilder.generateEntityFromDTO(currentPatient),
                            startTime,
                            endTime
                    );

                    Call<Integer> medPlanCall = DoctorService.getInstance().createMedicationPlan(newMedPlan);
                    medPlanCall.enqueue(new Callback<Integer>() {
                        @Override
                        public void onResponse(Call<Integer> call, Response<Integer> response) {
                            if(response.isSuccessful()) {
                                Log.d("Create medication plan", "onSuccess");
                                Toast.makeText(AddMedicationPlan.this, "Create medication plan successful! ", Toast.LENGTH_SHORT).show();
                                //Bundle bundle = new Bundle();
                                id = response.body();
                                //bundle.putInt("medPlanId", response.body());

                            } else {
                                Log.d("Create medication plan", "failed");
                                Toast.makeText(AddMedicationPlan.this, "Create medication plan failed", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Integer> call, Throwable t) {
                            Log.d("Find patient by id!", "onSuccess");
                        }
                    });


                } else {
                    Log.d("Find patient by id!", "failed");
                }
            }

            @Override
            public void onFailure(Call<PatientDTO> call, Throwable t) {
                Log.d("Find patient by id!", "onFailure");
            }
        });
    }

    public void goToAddMedToPlan(){

        System.out.println("MED PLAN ID:" + this.id);
        Intent intent = new Intent(AddMedicationPlan.this, AddMedicationToPlan.class);
        Log.d(TAG, "goToAddMedToPlan: medicationPlanId sending - is:"+ this.id);
        intent.putExtra("medicationPlanId", this.id);

        startActivityForResult(intent,1);
    }



    public void getAllMedicationsFromPlan() {
        System.out.println("Medication Plan Id is:"+ this.id);

        Call<List<MedicationPerPlanDTO>> medicationPlanDTOCall = DoctorService.getInstance().getMedicationsForPlan(this.id);

        medicationPlanDTOCall.enqueue(new Callback<List<MedicationPerPlanDTO>>() {
            @Override
            public void onResponse(Call<List<MedicationPerPlanDTO>> call, Response<List<MedicationPerPlanDTO>> response) {
                Log.d(TAG, "onResponse: getAllMedsFromPLan - getMedPlan");

                if(response.isSuccessful()) {
                    Log.d(TAG, "onResponse: getAllMedsFromPLan - getMedPlan successful ");
                    Log.d(TAG, "onResponse: Medications:");

                    List<MedicationPerPlanDTO> meds = response.body();

                    RecyclerView recyclerView = findViewById(R.id.recycler_doctor_med_plans);

                    MPMedicationAdapter adapter = new MPMedicationAdapter(meds, AddMedicationPlan.this);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(AddMedicationPlan.this));

                } else {
                    Log.d(TAG, "onResponse: getAllMedsFromPLan - getMedPlan failed");
                }
            }
            @Override
            public void onFailure(Call<List<MedicationPerPlanDTO>> call, Throwable t) {
                Log.d(TAG, "onFailure: getAllMedsFromPLan - getMedPlan");
            }
        });
    }
}
