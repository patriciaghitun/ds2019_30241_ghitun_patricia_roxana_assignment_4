package com.example.sd_android_project.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.dto.user.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class PatientAdapter extends RecyclerView.Adapter<PatientAdapter.PatientViewHolder>{

    private static final String TAG = "PatientAdapter";

    private List<PatientDTO> patientDTOS = new ArrayList<>();
    private Context context ;

    public PatientAdapter(List<PatientDTO> patientDTOS, Context context) {
        this.patientDTOS = patientDTOS;
        this.context = context;
    }

    @NonNull
    @Override
    public PatientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.allpatients_recycler_item, parent, false);

        PatientAdapter.PatientViewHolder patientViewHolder = new PatientAdapter.PatientViewHolder(view);
        return patientViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PatientViewHolder holder, int position) {
        holder.name.setText(patientDTOS.get(position).getUser().getName());
    }


    @Override
    public int getItemCount() {
        return patientDTOS.size();
    }

    public class PatientViewHolder extends RecyclerView.ViewHolder {

        TextView name;

        LinearLayout allpatientsLayout;

        public PatientViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.recycler_patient_name);
            allpatientsLayout = itemView.findViewById(R.id.allpatients_layout);
        }
    }
}