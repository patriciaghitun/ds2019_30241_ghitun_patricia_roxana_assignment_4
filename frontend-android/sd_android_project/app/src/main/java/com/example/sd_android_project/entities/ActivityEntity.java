package com.example.sd_android_project.entities;

import com.google.gson.annotations.SerializedName;

public class ActivityEntity {

    @SerializedName("id")
    private Integer id;

    @SerializedName("label")
    private String label;

    @SerializedName("start")
    private String start;

    @SerializedName("end")
    private String end;

    @SerializedName("patientid")
    private Integer patientId;

    @SerializedName("anomalous")
    private Boolean anomalous;

    @SerializedName("recommendation")
    private String recommendation;

    public ActivityEntity(Integer id, String label, String start, String end, Integer patientId, Boolean anomalous, String recommendation) {
        this.id = id;
        this.label = label;
        this.start = start;
        this.end = end;
        this.patientId = patientId;
        this.anomalous = anomalous;
        this.recommendation = recommendation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Boolean getAnomalous() {
        return anomalous;
    }

    public void setAnomalous(Boolean anomalous) {
        this.anomalous = anomalous;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    @Override
    public String toString() {
        return "ActivityEntity{" +
                "id=" + id +
                ", label='" + label + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", patientId=" + patientId +
                ", anomalous=" + anomalous +
                ", recommendation='" + recommendation + '\'' +
                '}';
    }
}