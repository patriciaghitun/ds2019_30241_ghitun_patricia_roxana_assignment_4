package soap.soapserver.builder;

import soap.soapserver.dto.ActivityDTO;
import soap.soapserver.entity.ActivityEntity;

public class ActivityBuilder {
    private ActivityBuilder() {
    }

    public static ActivityDTO generateDTOFromEntity(ActivityEntity activity) {
        return new ActivityDTO(
                activity.getId(),
                activity.getStart(),
                activity.getEnd(),
                activity.getLabel(),
                activity.getPatientId());
    }

    public static ActivityEntity generateEntityFromDTO(ActivityDTO activityDTO) {
        return new ActivityEntity(
                activityDTO.getId(),
                activityDTO.getStart(),
                activityDTO.getEnd(),
                activityDTO.getLabel(),
                activityDTO.getPatientId()
        );
    }
}
