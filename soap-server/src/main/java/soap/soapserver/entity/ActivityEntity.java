package soap.soapserver.entity;

import com.fasterxml.jackson.annotation.JsonProperty;


import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "activity")
public class ActivityEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @JsonProperty("startDate")
    @Column(name = "start", nullable = false,length = 200)
    private String start;

    @JsonProperty("endDate")
    @Column(name = "end", nullable = false,length = 200)
    private String end;

    @JsonProperty("label")
    @Column(name = "label", nullable = false,length = 200)
    private String label;

    @JsonProperty("patientId")
    @Column(name = "patientid", nullable = false)
    private Integer patientId;

    public ActivityEntity() {}

    public ActivityEntity(@JsonProperty("patientId") int id,
                          @JsonProperty("label") String activity,
                          @JsonProperty("startDate") String start,
                          @JsonProperty("endDate") String end
    ) {
        this.patientId = id;
        this.label = activity;
        this.start = start;
        this.end = end;
    }
    public ActivityEntity(Integer id, String start, String end, String label, Integer patient) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.label = label;
        this.patientId = patient;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }


    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", label='" + label + '\'' +
                ", patientId=" + patientId +
                '}';
    }
}
