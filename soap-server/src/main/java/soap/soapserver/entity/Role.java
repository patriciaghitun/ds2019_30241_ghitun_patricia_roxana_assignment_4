package soap.soapserver.entity;


public enum Role {
    DOCTOR,
    CAREGIVER,
    PATIENT
}
