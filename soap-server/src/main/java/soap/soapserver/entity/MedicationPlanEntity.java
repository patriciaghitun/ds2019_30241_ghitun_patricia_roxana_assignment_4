package soap.soapserver.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication_plan")
public class MedicationPlanEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @NotNull
    @ManyToOne(cascade = CascadeType.REMOVE)//Many medicationPlan to one patient - cascade type remove =>
    // trebuie sa am userul in baza de date inainte ca sa pot adauga un nou medication plan cu patient-ul ala
    @JoinColumn(name = "patient_id")
    private PatientEntity patient; // informatii despre userul respectiv

    //perioada
    @Column(name = "start", length = 200)
    private String start;

    @Column(name = "end", length = 200)
    private String end;

    public MedicationPlanEntity() {}

    public MedicationPlanEntity(Integer id, PatientEntity patient, String start, String end) {
        this.id = id;
        this.patient = patient;
        this.start = start;
        this.end = end;
    }

    public PatientEntity getPatient() {
        return patient;
    }

    public void setPatient(PatientEntity patient) {
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "MedicationPlan{" +
                "id=" + id +
                ", patient=" + patient +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}