package soap.soapserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import soap.soapserver.builder.ActivityBuilder;
import soap.soapserver.dto.ActivityDTO;
import soap.soapserver.entity.ActivityEntity;
import soap.soapserver.repository.ActivityRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ActivityService {

    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public ActivityDTO findActivityById(Integer id){
        Optional<ActivityEntity> activity  = activityRepository.findById(id);

        return ActivityBuilder.generateDTOFromEntity(activity.get());
    }

    //CRUD

    public List<ActivityDTO> findAll(){
        List<ActivityEntity> activities = activityRepository.getAllOrdered();

        return activities.stream()
                .map(ActivityBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(ActivityDTO activityDTO) {
        return activityRepository
                .save(ActivityBuilder.generateEntityFromDTO(activityDTO))
                .getId();
    }

    public Integer update(ActivityDTO activityDTO) {

        Optional<ActivityEntity> activity = activityRepository.findById(activityDTO.getId());

        return activityRepository.save(ActivityBuilder.generateEntityFromDTO(activityDTO)).getId();
    }

    public void deleteById(Integer id) {
        this.activityRepository.deleteById(id);
    }


    public List<ActivityDTO> getAllByPatientId(Integer patientId) {
        List<ActivityEntity> activities = activityRepository.getAllByPatientIdOrderByStartAsc(patientId);

        return activities.stream()
                .map(ActivityBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public ActivityDTO findActivityByLabel(String label) {
        return ActivityBuilder.generateDTOFromEntity(activityRepository.findActivityEntityByLabel(label));
    }
}
