package soap.soapserver.dto;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityDTO {

    private Integer id;

    private String start;
    private String end;
    private String label;
    private Integer patientId;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public ActivityDTO() {}

    public ActivityDTO(Integer id, String start, String end, String label, Integer patientId) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.label = label;
        this.patientId = patientId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public long getDurationByHours() throws ParseException {

        Date startDate, endDate;

        startDate = formatter.parse(this.start);
        endDate = formatter.parse(this.end);

        long diff = endDate.getTime() - startDate.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        return diffHours;
    }

    @Override
    public String toString() {
        return "ActivityDTO{" +
                "id=" + id +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", label='" + label + '\'' +
                ", patientId=" + patientId +
                ", formatter=" + formatter +
                '}';
    }
}