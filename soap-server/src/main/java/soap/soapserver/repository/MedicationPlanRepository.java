package soap.soapserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import soap.soapserver.entity.MedicationPlanEntity;

import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlanEntity, Integer> {
    List<MedicationPlanEntity> getAllByPatientId(Integer id);
}
